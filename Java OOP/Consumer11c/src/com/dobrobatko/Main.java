package com.dobrobatko;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.BiConsumer;

public class Main {

	public static void main(String[] args) {
		
		BiConsumer<String, File> biCon = Main::stringToFile;
		
		File file = new File("a.txt");
		String str = "New repeat => ";
		
		int repeats = 10;
		
		for(int i=0; i < repeats; i++) biCon.accept(str + (i + 1), file);
		
	}
	
	public static void stringToFile (String str, File file) {
		try(FileWriter fw = new FileWriter(file, true);
			    BufferedWriter bw = new BufferedWriter(fw)) { 
				
			bw.write(str);
			bw.write(System.lineSeparator());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
