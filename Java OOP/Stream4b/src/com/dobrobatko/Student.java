package com.dobrobatko;

public class Student extends Human {
	private String faculty;
	private String group;
	private double averageScore;

	public Student(String name, String lastName, int age, String sex, String city, String faculty, String group,
			double averageScore) {
		super(name, lastName, age, sex, city);
		this.faculty = faculty;
		this.group = group;
		this.averageScore = averageScore;
	}

	public Student(String name, String lastName, int age, String sex, String city, String faculty, String group) {
		super(name, lastName, age, sex, city);
		this.faculty = faculty;
		this.group = group;
	}

	public Student(String name, String lastName, int age, String sex, String faculty, String group,
			double averageScore) {
		super(name, lastName, age, sex);
		this.faculty = faculty;
		this.group = group;
		this.averageScore = averageScore;
	}

	public Student(String name, String lastName, int age, String sex, String faculty, String group) {
		super(name, lastName, age, sex);
		this.faculty = faculty;
		this.group = group;
	}

	public Student() {
		super();
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public double getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(double averageScore) {
		this.averageScore = averageScore;
	}

	@Override
	public String toString() {
		return super.toString() + " Student [faculty=" + faculty + ", group=" + group + ", averageScore=" + averageScore
				+ " ]";
	}
}
