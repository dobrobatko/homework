package com.dobrobatko;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Student cl = new Student("Charle", "Leclerc", 18, "male", "Extreme driving", null);
		Student cs = new Student("Carlos", "Sainz", 19, "male", "Extreme driving", null);
		Student lh = new Student("Lewis", "Hamilton", 34, "male", "Extreme driving", null);
		Student mv = new Student("Max", "Verstappen", 18, "male", "Extreme driving", null);
		Student fa = new Student("Fernando", "Alonso", 34, "male", "Extreme driving", null);
		Student cp = new Student("Checo", "Perez", 32, "male", "Extreme driving", null);
		Student sv = new Student("Sebastian", "Vettel", 35, "male", "Extreme driving", null);

		Student[] freshmans = { cl, cs, lh, mv, fa, cp, sv };
		
		List<Student> oldest = Arrays.stream(freshmans)
				.filter(s -> s.getAge() > 20)
				.sorted((a,b) -> a.getLastName().compareTo(b.getLastName()))
				.toList();
		
		for (Student student : oldest) {
			System.out.println(student);
		}

	}

}
