package com.dobrobatko;

import java.util.ArrayDeque;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		ArrayDeque<BigBangPerson> tail = new ArrayDeque<>();

		tail.add(new BigBangPerson("Sheldon"));
		tail.add(new BigBangPerson("Leonard"));
		tail.add(new BigBangPerson("Volovitc"));
		tail.add(new BigBangPerson("Kutrapalli"));
		tail.add(new BigBangPerson("Penny"));

		for (BigBangPerson bigBangPerson : tail) {
			System.out.print(bigBangPerson + ", ");
		}
		System.out.println();

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of cola glasses: ");
		int glasses = sc.nextInt();

		for (int i = 1; i <= glasses; i++) {
			BigBangPerson temporary = tail.pop();
			tail.add(temporary);
			tail.add(temporary.clone());
		}

		for (BigBangPerson bigBangPerson : tail) {
			System.out.print(bigBangPerson + ", ");
		}
	}

}
