package com.dobrobatko;

public class BigBangPerson implements Cloneable {
	private String nickName;

	public BigBangPerson(String nickName) {
		super();
		this.nickName = nickName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return nickName;
	}

	@Override
	public BigBangPerson clone() {
		try {
			return (BigBangPerson) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

}
