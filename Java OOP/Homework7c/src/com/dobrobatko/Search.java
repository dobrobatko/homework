package com.dobrobatko;

import java.io.File;

public class Search implements Runnable {
	private File file;
	private String name;

	public Search(String name, File file) {
		super();
		this.name = name;
		this.file = file;
	}

	@Override
	public void run() {
		Finder.findFile(name, file);
	}
}
