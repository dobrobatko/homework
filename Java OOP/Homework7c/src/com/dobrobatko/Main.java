package com.dobrobatko;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {

		File startFolder = new File(".");
		String name = "a.txt";

		File[] folders = startFolder.listFiles();

		ExecutorService threads = Executors.newFixedThreadPool(2);
		Runnable[] runs = new Runnable[folders.length];

		for (int i = 0; i < folders.length; i++) {
			runs[i] = new Search(name, folders[i]);
			threads.execute(runs[i]);
		}
		threads.shutdown();
	}
}
