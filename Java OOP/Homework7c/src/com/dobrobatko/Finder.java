package com.dobrobatko;

import java.io.File;

public class Finder {
	public Finder() {
		super();
	}

	public static void findFile(String name, File folder) {
		if (folder.isFile()) {
			if (folder.getName().equals(name)) {
				System.out.println(folder);
			}
		} else {
			File[] files = folder.listFiles();
			for (int i = 0; i < files.length; i++) {
				findFile(name, files[i]);
			}
		}
	}
}
