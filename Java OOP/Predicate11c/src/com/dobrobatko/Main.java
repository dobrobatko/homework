package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

public class Main {

	public static void main(String[] args) {

		BiPredicate<Cat, String> pr2 = Main::filterName;
		BiPredicate<Cat, Integer> pr1 = Main::filterAge;

		Cat cat1 = new Cat("Umka", 12);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 8);
		Cat cat4 = new Cat("Timka", 4);
		Cat cat5 = new Cat("Kuzia", 2);

		List<Cat> cats = new ArrayList<>(List.of(cat1, cat2, cat3, cat4, cat5));
		System.out.println(cats);
		
		List<Cat> removedCats = new ArrayList<>();

		for (Cat cat : cats) {
			if (pr2.test(cat, "C") && pr1.test(cat, 5)) {
				removedCats.add(cat);
			}
		}

		cats.removeAll(removedCats);

		System.out.println(cats);

	}

	public static boolean filterAge(Cat cat, Integer age) {
		return cat.getAge() < age;
	}

	public static boolean filterName(Cat cat, String letter) {
		char startName = cat.getName().charAt(0);
		return (startName > letter.charAt(0));
	}

}
