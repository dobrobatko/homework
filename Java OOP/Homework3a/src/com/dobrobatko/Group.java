package com.dobrobatko;

public class Group {
	private String name;
	private Student[] students = new Student[10];

	public Group(String name, Student[] students) {
		super();
		this.name = name;
		this.students = students;
	}

	public Group(String name) {
		super();
		this.name = name;
	}

	public Group() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student[] getStudents() {
		return students;
	}

	/*
	 * public void setStudents(Student[] students) { this.students = students; }
	 */

	public void add(Student student) throws OverloadGroupException {
		boolean flag = false;
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				students[i].setGroup(name);
				flag = true;
				break;
			}
		}
		if (flag != true) {
			throw new OverloadGroupException();
		}
	}

	public void add(Student[] fewStudents) throws OverloadGroupException {
		if (fewStudents.length > students.length) {
			throw new OverloadGroupException();
		}
		for (int i = 0; i < fewStudents.length; i++) {
			add(fewStudents[i]);
		}
	}

	public Student findByLastName(String lastName) {
		for (Student student : students) {
			if (student.getLastName() == lastName) {
				return student;
			}
		}
		return null;
	}

	private Student[] bubbleSort(Student[] array) {
		for (int j = 0; j < array.length; j++) {
			for (int i = 0; i < array.length - 1; i++) {
				Student temp = new Student();
				if (i != array.length && array[i + 1] != null && array[i] != null) {
					String lastNameA = array[i].getLastName();
					char chA = lastNameA.charAt(0);
					String lastNameB = array[i + 1].getLastName();
					char chB = lastNameB.charAt(0);
					if (chB < chA) {
						temp = array[i];
						array[i] = array[i + 1];
						array[i + 1] = temp;
					}
				}
			}
		}
		return array;
	}
	
	private int countMembers (Human[] array) {
		int count = 0;
		for (Human person : array) {
			if(person != null) {
				count++;
			}
		}
		return count;
	}

	@Override
	public String toString() {
		students = bubbleSort(students);
		StringBuilder sb = new StringBuilder("Group " + name + " consists of " + countMembers(students) + " members:");
		sb.append(System.lineSeparator());
		for (Student student : students) {
			if (student != null) {
				sb.append(student.toString());
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}

}
