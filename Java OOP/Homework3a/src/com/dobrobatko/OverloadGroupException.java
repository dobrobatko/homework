package com.dobrobatko;

public class OverloadGroupException extends Exception {

	@Override
	public String getMessage() {
		return "Too many students for this group.";
	}
}
