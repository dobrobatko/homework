package com.dobrobatko;

import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Apocalypse", 2,4);
		Cat cat2 = new Cat("Luska", 5,5);
		Cat cat3 = new Cat("Barsic", 8,8);
		Cat cat4 = new Cat("Timka", 5,5);
		Cat cat5 = new Cat("Kuzia", 2,2);
		Cat cat6 = new Cat("Umka", 12,3);
		
		List<Cat> cats = List.of(cat1, cat2, cat3, cat4, cat5, cat6);
		
		Comparator<Cat> compName = (a,b) -> a.getName().compareTo(b.getName());
		
		List<Cat> heavyWeightCats = cats.stream()
				.filter(cat -> cat.getWeight() > 3)
				.sorted(compName)
				.toList();
		
		System.out.println(heavyWeightCats);

	}

}
