package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		
		int[] mainArray = new int[200000000];
		int[] arr1 = new int[50000000];
		int[] arr2 = new int[50000000];
		int[] arr3 = new int[50000000];
		int[] arr4 = new int[50000000];
		
		for (int i = 0; i < mainArray.length; i++) {
			mainArray[i] = (int)(Math.random()*10);
		}
		
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Total execution time to create 200M objects in Array in millis: "
                + elapsedTime/1000000);
		
		startTime = System.nanoTime();
		
		int summ = 0;
		for (int i : mainArray) {
			summ += i;
		}
		System.out.println(summ);
		
		elapsedTime = System.nanoTime() - startTime;
		System.out.println("Total execution time to summ(one thread) 200M objects in Array in millis: "
                + elapsedTime/1000000);
		
		startTime = System.nanoTime();
		
		partArray(mainArray, arr1, 1);
		partArray(mainArray, arr2, 2);
		partArray(mainArray, arr3, 3);
		partArray(mainArray, arr4, 4);
		
		SumArray sa1 = new SumArray(arr1);
		SumArray sa2 = new SumArray(arr2);
		SumArray sa3 = new SumArray(arr3);
		SumArray sa4 = new SumArray(arr4);
		
		Thread th1 = new Thread(sa1);
		Thread th2 = new Thread(sa2);
		Thread th3 = new Thread(sa3);
		Thread th4 = new Thread(sa4);
		
		th1.start();
		th2.start();
		th3.start();
		th4.start();
		
		try {
			th1.join();
			th2.join();
			th3.join();
			th4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		summ = (sa1.getSumm() + sa2.getSumm() + sa3.getSumm() + sa4.getSumm());
		System.out.println(summ);
		
		elapsedTime = System.nanoTime() - startTime;
		System.out.println("Total execution time to summ(four threads) 200M objects in Array in millis: "
                + elapsedTime/1000000);
		
	}
	
	public static void partArray(int[] main, int[] copy, int part) {
		int begin = 0;
		
		switch (part) {
		case 1:
			begin = 0;
			break;
		case 2:
			begin = 50000000;
			break;
		case 3:
			begin = 100000000;
			break;
		case 4:
			begin = 150000000;
			break;
		default:
			System.out.println("Unknown part!");
			break;
		}
		if(part>0 && part<5) {
			System.arraycopy(main, begin, copy, 0, 50000000);
		}
		
	}
	
	

}
