package com.dobrobatko;

public class SumArray implements Runnable {
	private int[] array;
	private int summ = 0;

	public SumArray(int[] array) {
		super();
		this.array = array;
	}

	private void calculate() {
		for (int element : array) {
			summ += element;
		}
	}

	@Override
	public void run() {
		String thName = Thread.currentThread().getName();
		calculate();
		System.out.println("Thread: " + thName + " finished.");
		
	}

	public int getSumm() {
		return summ;
	}

}
