package com.dobrobatko;

import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<Integer> mainList = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -1);
		
		List<Integer> oddList = mainList.stream()
				.filter(a -> a%2 != 0)
				.sorted()
				.collect(Collectors.toList());
		
		System.out.println(oddList);
	}

}
