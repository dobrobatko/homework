package com.dobrobatko;

import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		Supplier<String> word = new WordFromString("Hello Java world");
		String anotherWord;
		for (; (anotherWord = word.get()) != null;) {
			System.out.println(anotherWord);
		}

	}

}

class WordFromString implements Supplier<String> {
	private String common;
	private int index = -1;

	public WordFromString(String common) {
		super();
		this.common = common;
	}

	@Override
	public String get() {
		String[] words = common.split(" ");
		if (index >= words.length - 1) {
			return null;
		}
		index++;
		return words[index];
	}

}
