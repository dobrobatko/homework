package com.dobrobatko;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<Integer> numbers = List.of(7, -8, 3, -3, 5, -10, 22);

		Comparator<Integer> comp1 = Main::nearZero;
		Comparator<Integer> comp2 = Main::ifSame;

		Integer near = Collections.min(numbers, comp1.thenComparing(comp2));
		System.out.println(near);

	}

	public static int nearZero(Integer a, Integer b) {
		if (Math.abs(a) > Math.abs(b)) {
			return 1;
		}
		if (Math.abs(a) < Math.abs(b)) {
			return -1;
		}
		return 0;
	}

	public static int ifSame(Integer a, Integer b) {
		return Math.abs(a);
	}
}
