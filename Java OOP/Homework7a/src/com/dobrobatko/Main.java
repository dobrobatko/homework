package com.dobrobatko;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Main {

	public static void main(String[] args) {
		ExecutorService docks = Executors.newFixedThreadPool(2);
		
		Runnable dis1 = new Discharge("Paola", 20);
		Runnable dis2 = new Discharge("Victoriya", 10);
		Runnable dis3 = new Discharge("Seren", 15);
		
		docks.execute(dis1);
		docks.execute(dis2);
		docks.execute(dis3);
		
		docks.shutdown();
		
	}

}
