package com.dobrobatko;

public class Discharge implements Runnable {
	private int box;
	private String ship;
	private int time = 500;

	public Discharge(String ship, int box) {
		super();
		this.ship = ship;
		this.box = box;
	}

	public Discharge() {
		super();
	}

	public int getBox() {
		return box;
	}

	@Override
	public void run() {
		System.out.println(this.ship + " start discharging in dock.");
		for (; box > 0;) {
			try {
				Thread.currentThread().sleep(time);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			box--;
			System.out.println(this.box + " boxes left on " + this.ship);
		}
		System.out.println("Ship " + this.ship + " discharged!");
	}

}
