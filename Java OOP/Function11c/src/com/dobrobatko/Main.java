package com.dobrobatko;

import java.util.function.Function;

public class Main {

	public static void main(String[] args) {

		Function<String, char[]> fun1 = a -> a.toCharArray();
		Function<char[], Integer> fun2 = a -> {
			Integer count = 0;
			for (char ch : a) {
				count += ch;
			}
			return count;
		};
		Function<String, Integer> fun3 = fun1.andThen(fun2);

		System.out.println(fun3.apply("Hello Java"));
	}

}
