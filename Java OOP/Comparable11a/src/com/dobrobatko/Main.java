package com.dobrobatko;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		
		
		Rectangle rectOne = new Rectangle(10, 5);
		Rectangle rectTwo = new Rectangle(10, 7);
		Rectangle rectThree = new Rectangle(8, 3);
		Rectangle rectFour = new Rectangle(12, 2);
		Rectangle rectFive = new Rectangle(5, 10);
		
		Rectangle[] array = new Rectangle [] {rectOne, rectTwo, rectThree, rectFour, rectFive};
		
		for (Rectangle rectangle : array) {
			System.out.println(rectangle);
		}
		
		System.out.println();
		Arrays.sort(array);
		
		for (Rectangle rectangle : array) {
			System.out.println(rectangle);
		}

	}

}
