package com.dobrobatko;

public class Rectangle implements Comparable<Rectangle> {
	private double width;
	private double height;

	public Rectangle(double width, double height) {
		super();
		this.width = width;
		this.height = height;
	}

	public Rectangle() {
		super();

	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getArea() {
		return width * height;
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", height=" + height + ", getArea()=" + getArea() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rectangle other = (Rectangle) obj;
		return Double.doubleToLongBits(width) == Double.doubleToLongBits(other.width)
				&& Double.doubleToLongBits(height) == Double.doubleToLongBits(other.height);
	}

	@Override
	public int compareTo(Rectangle o) {
		if (o == null) {
			throw new NullPointerException();
		}
		if (this.getArea() > o.getArea()) {
			return 1;
		}
		if (this.getArea() < o.getArea()) {
			return -1;
		}
		if (this.getArea() == o.getArea() && this.height > o.height) {
			return 1;
		}
		if (this.getArea() == o.getArea() && this.height < o.height) {
			return -1;
		}
		return 0;
	}

}
