package com.dobrobatko;

import java.util.function.IntPredicate;

public class Main {

	public static void main(String[] args) {

		IntPredicate intPr = Main::isSumEven;

		System.out.println(intPr.test(103));
		System.out.println(intPr.test(103431679));
		System.out.println(intPr.test(104));

	}

	public static boolean isSumEven(int number) {
		String num = String.valueOf(number);
		char[] cips = num.toCharArray();
		int sum = 0;
		for (int i = 0; i < cips.length; i++) {
			sum += Integer.valueOf(cips[i]);
		}
		if (sum % 2 == 0) {
			return true;
		}
		return false;
	}

}
