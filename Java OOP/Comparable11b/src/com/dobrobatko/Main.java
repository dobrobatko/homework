package com.dobrobatko;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Apocalypse", 2);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 8);
		Cat cat4 = new Cat("Timka", 5);
		Cat cat5 = new Cat("Kuzia", 2);
		Cat cat6 = new Cat("Umka", 12);

		Cat cat7 = new Cat("Barsic", 8);
		Cat cat8 = new Cat("Martin", 8);

		System.out.println(cat3.equals(cat7)); // the same cat
		System.out.println(cat3.compareTo(cat7)); // the same cat
		System.out.println(cat3.equals(cat8)); // the same name.length and the same age, but different cats
		System.out.println(cat3.compareTo(cat8)); // the same name.length and the same age, but different cats

		Cat[] cats = new Cat[] { cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8 };

		for (Cat cat : cats) {
			System.out.println(cat);
		}
		System.out.println();

		Arrays.sort(cats);

		for (Cat cat : cats) {
			System.out.println(cat);
		}
		System.out.println();

		Cat max = null;
		max = max(cats);
		System.out.println("Max cat is " + max);

	}

	public static <T extends Comparable<T>> T max(T[] array) {
		T max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (max.compareTo(array[i]) < 0) {
				max = array[i];
			}
		}
		return max;
	}

}
