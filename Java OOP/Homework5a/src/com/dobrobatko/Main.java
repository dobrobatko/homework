package com.dobrobatko;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File input = new File("input");
		File output = new File("output");

		CustomFileFilter cFF = new CustomFileFilter("doc");

		File[] filteredFiles = input.listFiles(cFF);
		for (File file : filteredFiles) {
			File out = new File(output.getName() + "\\" + file.getName());
			try {
				FileOperation.fileCopy(file, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
