package com.dobrobatko;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

public class Main {

	public static void main(String[] args) {
		HashMap<Integer, String> myMap = new HashMap<>(Map.of(1, "Hello", 2, "world", 3, "I", 4, "like", 5, "Java"));

		BiPredicate<Integer, String> biPr = (a, b) -> a == b.length();
		List<Integer> removedKeys = new ArrayList<>();

		for (Map.Entry<Integer, String> entry : myMap.entrySet()) {
			Integer key = entry.getKey();
			String val = entry.getValue();

			if (!biPr.test(key, val))
				removedKeys.add(key);

		}

		for (Integer key : removedKeys) {
			myMap.remove(key);
		}

		System.out.println(myMap);

	}

}
