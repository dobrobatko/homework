package com.dobrobatko;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileOperation {
	
	public FileOperation() {
		super();
	}

	public static String readFile (String path) {
		File file = new File(path);
		String line = "";
		try (BufferedReader f = new BufferedReader(new FileReader(file))) {
			String str = "";
			for (; (str = f.readLine()) != null;) {
				line += str + " ";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}
	
	public static void writeFile (String path, String text ) {
		File file = new File(path);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(text);
			System.out.println("The result was successfully written.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
