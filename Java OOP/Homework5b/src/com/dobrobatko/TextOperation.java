package com.dobrobatko;

public class TextOperation {

	public TextOperation() {
		super();
	}

	public static String filterDuplicatedWords(String line1, String line2) {

		String[] words1 = line1.split(" ");
		String[] words2 = line2.split(" ");

		for (int i = 0; i < words1.length; i++) {
			words1[i] = removeChar(words1[i], '.', ':', ';', ',');
		}

		for (int i = 0; i < words2.length; i++) {
			words2[i] = removeChar(words2[i], '.', ':', ';', ',');
		}

		String result = "";

		for (int i = 0; i < words1.length; i++) {
			for (int j = 0; j < words2.length; j++) {
				int index;
				if (words1[i].equals(words2[j]) && (index = result.indexOf(words1[i])) == -1) {
					result += words1[i] + " ";
				}
			}
		}
		return result;
	}

	private static String removeChar(String word, char... chs) {
		for (char ch : chs) {
			int index = word.lastIndexOf(ch);
			if (index != -1) {
				StringBuffer sb = new StringBuffer(word);
				sb.delete(index, index + 1);
				return word = sb.toString();
			}
		}
		return word;
	}

}
