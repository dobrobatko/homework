package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		String string1 = FileOperation.readFile("1.txt");
		String string2 = FileOperation.readFile("2.txt");

		String result = TextOperation.filterDuplicatedWords(string1, string2);

		FileOperation.writeFile("3.txt", result);
	}
}
