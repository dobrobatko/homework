package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		
		
		// Online Shop Items
		Goods item1 = new Goods("Nokia 3310", 0.133);
		item1.setInfo("The best model ever!");
		
		Goods item2 = new Goods();
		item2.setName("Samsung Galaxy S22 Ultra");
		item2.setWeight(0.228);
		item2.setInfo("Not bad too...");
		
		System.out.println(item1.toString());
		System.out.println(item2.toString());
		
		//Triangle
		Triangle abc = new Triangle(20, 30, 40);
		System.out.println(abc.toString());

	}

}
