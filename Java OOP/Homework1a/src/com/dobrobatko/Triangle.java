package com.dobrobatko;

public class Triangle {
	private double aSide;
	private double bSide;
	private double cSide;
	
	public Triangle(double aSide, double bSide, double cSide) {
		super();
		this.aSide = aSide;
		this.bSide = bSide;
		this.cSide = cSide;
	}

	public double getASide() {
		return aSide;
	}

	public void setASide(double aSide) {
		this.aSide = aSide;
	}

	public double getBSide() {
		return bSide;
	}

	public void setBSide(double bSide) {
		this.bSide = bSide;
	}

	public double getCSide() {
		return cSide;
	}

	public void setCSide(double cSide) {
		this.cSide = cSide;
	}
	
	public double getArea() {
		double p = (aSide + bSide + cSide) / 2.0;
		double area = Math.sqrt(p * (p - aSide) * (p - bSide) * (p - cSide));
		return area;
	}

	@Override
	public String toString() {
		return "Triangle [aSide=" + aSide + ", bSide=" + bSide + ", cSide=" + cSide + ", Area=" + getArea() + "]";
	}
	
	
	
	
	
	
}
