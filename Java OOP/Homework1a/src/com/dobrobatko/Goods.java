package com.dobrobatko;

public class Goods {
	private String name;
	private double weight;
	private String info;
	
	public Goods(String name, double weight, String info) {
		super();
		this.name = name;
		this.weight = weight;
		this.info = info;
	}

	public Goods(String name, double weight) {
		super();
		this.name = name;
		this.weight = weight;
	}

	public Goods() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "Goods [name=" + name + ", weight=" + weight + " kg, info=" + info + "]";
	}
}
