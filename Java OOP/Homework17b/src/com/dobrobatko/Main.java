package com.dobrobatko;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		String url = "https://ua.korrespondent.net/";
		String file = "links.txt";
		String content = getContent(url);
		Set<String> links = new TreeSet<>(linksExtract(content));
		boolean complete = linksToFile(links, file);
		if (complete) {
			System.out.println("Links were successfully written.");
		} else {
			System.out.println("No links found.");
		}

	}

	public static String getContent(String urlAdress) {
		StringBuilder sb = new StringBuilder("");
		try {
			int c;
			URL url = new URL(urlAdress);
			URLConnection urlc = url.openConnection();
			long l = urlc.getContentLengthLong();
			if (l != 0) {
				InputStream ins = urlc.getInputStream();
				for (; (c = ins.read()) != -1;) {
					sb.append((char) c);
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return sb.toString();
	}

	public static Set<String> linksExtract(String content) {
		Set<String> links = new TreeSet<>();
		String[] blocks = content.split("<a href=\"");
		for (int i = 0; i < blocks.length; i++) {
			if (blocks[i].startsWith("http")) {
				int index = blocks[i].indexOf("\"");
				blocks[i] = blocks[i].substring(0, index);
				links.add(blocks[i]);
			}
		}
		return links;
	}

	public static boolean linksToFile(Set<String> links, String fileName) {
		boolean complete = false;
		File file = new File(fileName);
		Iterator<String> itr = links.iterator();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (; itr.hasNext();) {
				bw.write(itr.next());
				bw.write(System.lineSeparator());
			}
			complete = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return complete;
	}

}
