package com.dobrobatko;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Comparator<Integer> comp = Main::minSimple;

		List<Integer> list1 = List.of(50,10,131,44,80,11,7,64,40); //list with simple numbers
		Integer max = Collections.max(list1, comp);
		System.out.println(max);
		
		List<Integer> list2 = List.of(50,10,44,80,64,40); //list without simple numbers
		Integer min = Collections.max(list2, comp);
		System.out.println(min);

	}

	public static boolean isSimple(Integer num) {
		boolean flag = true;
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				flag = false;
				break;
			}
		}
		return flag;
	}

	public static Integer minSimple(Integer a, Integer b) {
		if (isSimple(a) && !isSimple(b)) {
			return 1;
		}
		if (!isSimple(a) && isSimple(b)) {
			return -1;
		}
		if (isSimple(a) && isSimple(b)) {
			return a - b;
		}
		if (!isSimple(a) && !isSimple(b)) {
			return b - a;
		}
		return 0;
	}

}
