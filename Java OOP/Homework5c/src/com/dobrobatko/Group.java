package com.dobrobatko;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public class Group {
	private String name;
	private Student[] students = new Student[10];

	public Group(String name, Student[] students) {
		super();
		this.name = name;
		this.students = students;
	}

	public Group(String name) {
		super();
		this.name = name;
	}

	public Group() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student[] getStudents() {
		return students;
	}

	/*
	 * public void setStudents(Student[] students) { this.students = students; }
	 */

	public void add(Student student) throws OverloadGroupException {
		boolean flag = false;
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				students[i].setGroup(name);
				flag = true;
				break;
			}
		}
		if (flag != true) {
			throw new OverloadGroupException();
		}
	}

	public void add(Student[] fewStudents) throws OverloadGroupException {
		if (fewStudents.length > students.length) {
			throw new OverloadGroupException();
		}
		for (int i = 0; i < fewStudents.length; i++) {
			add(fewStudents[i]);
		}
	}

	public Student findByLastName(String lastName) {
		for (Student student : students) {
			if (student.getLastName() == lastName) {
				return student;
			}
		}
		return null;
	}

	private int countMembers(Human[] array) {
		int count = 0;
		for (Human person : array) {
			if (person != null) {
				count++;
			}
		}
		return count;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Group " + name + " consists of " + countMembers(students) + " members:");
		sb.append(System.lineSeparator());
		for (Student student : students) {
			if (student != null) {
				sb.append(student.toString());
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}

	public void sortStudentsByLastName() {
		Arrays.sort(students, Comparator.nullsLast(new LastNameComparator()));
	}

	public static Group[] acceptToGroup(Group[] array, Student newer) throws OverloadGroupException {
		for (int i = 0; i < array.length; i++) {
			if (array[i].getName().equals(newer.getGroup())) {
				array[i].add(newer);
				System.out.println("Student added to an existing group: " + array[i].getName());
				return array;
			}
		}
		Group firstCurse = new Group(newer.getGroup());
		firstCurse.add(newer);
		Group[] arr = new Group[array.length + 1];
		System.arraycopy(array, 0, arr, 0, array.length);
		arr[array.length] = firstCurse;
		System.out.println("Student added. New group " + firstCurse.getName() + " created");
		return arr;
	}

	public void exportGroupToFile(String file) {
		File f = new File(file);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
			for (Student student : students) {
				if (student != null) {
					String line = student.toCSVString();
					bw.write(line);
					bw.append(System.lineSeparator());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void importGroupFromFile(String file) throws OverloadGroupException {
		File f = new File(file);
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String str = "";
			for (; (str = br.readLine()) != null;) {
				Student line = new Student();
				line = line.fromCSVString(str);
				boolean flag = false;
				for (int i = 0; i < students.length; i++) {
					if (students[i] == null) {
						students[i] = line;
						students[i].setGroup(name);
						flag = true;
						break;
					}
				}
				if (flag != true) {
					throw new OverloadGroupException();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
