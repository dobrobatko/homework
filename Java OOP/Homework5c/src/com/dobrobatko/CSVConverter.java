package com.dobrobatko;

public interface CSVConverter {
	public String toCSVString();
	public Student fromCSVString (String str);
}
