package com.dobrobatko;

public class CancelInputException extends Exception {

	@Override
	public String getMessage() {
		return "Input is cancelled. The student has not been added.";
	}
}
