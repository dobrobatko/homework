package com.dobrobatko;

import java.util.List;
import java.util.Optional;

public class Main {

	public static void main(String[] args) {
		
		Cat cat1 = new Cat("Apocalypse", 2);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 8);
		Cat cat4 = new Cat("Timka", 5);
		Cat cat5 = new Cat("Kuzia", 2);
		Cat cat6 = new Cat("Umka", 12);
		
		List<Cat> cats = List.of(cat1, cat2, cat3, cat4, cat5, cat6);
		
		Optional<String> result = cats.stream()
		.max((a,b) -> a.getName().length() - b.getName().length())
		.map(a -> a.getName());
		
		System.out.println(result.get());
	}

}
