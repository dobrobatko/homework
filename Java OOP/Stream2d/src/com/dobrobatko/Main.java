package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		File pom = new File("pom.xml");
		List<String> values = null;

		try {
			values = Files.lines(pom.toPath())
					.map(a -> a.strip())
					.filter(a -> a.startsWith("<groupId>"))
					.map(a -> a.replace("<groupId>", ""))
					.map(a -> a.replace("</groupId>", ""))
					.toList();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(values);
	}

}
