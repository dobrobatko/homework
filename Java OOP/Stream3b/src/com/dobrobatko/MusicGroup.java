package com.dobrobatko;

import java.util.Arrays;

public class MusicGroup {
	private String name;
	private String[] songs;
	
	public MusicGroup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MusicGroup(String name, String[] songs) {
		super();
		this.name = name;
		this.songs = songs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getSongs() {
		return songs;
	}

	public void setSongs(String[] songs) {
		this.songs = songs;
	}

	@Override
	public String toString() {
		return "MusicGroup [name=" + name + ", songs=" + Arrays.toString(songs) + "]";
	}
	
	
}
