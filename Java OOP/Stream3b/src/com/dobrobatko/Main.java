package com.dobrobatko;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		MusicGroup metallica = new MusicGroup("Metallica", new String[] { "Seek & Destroy", "Master of Puppets" });
		MusicGroup acdc = new MusicGroup("AC/DC", new String[] { "Hells Bells", "Thunderstruck" });
		MusicGroup doors = new MusicGroup("Doors", new String[] { "Waiting for the Sun", "Roadhouse Blues" });

		MusicGroup[] groups = new MusicGroup[] { metallica, acdc, doors };

		List<String> songs = Arrays.stream(groups)
				.flatMap(n -> Arrays.stream(n.getSongs()))
				.sorted()
				.limit(3)
				.toList();

		System.out.println(songs);

	}

}
