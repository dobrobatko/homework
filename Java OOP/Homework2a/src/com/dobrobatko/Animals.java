package com.dobrobatko;

public class Animals {
	private String ration;
	private String color;
	private int weight;
	
	public Animals(String ration, String color, int weight) {
		super();
		this.ration = ration;
		this.color = color;
		this.weight = weight;
	}

	public Animals() {
		super();
	}

	public String getRation() {
		return ration;
	}

	public void setRation(String ration) {
		this.ration = ration;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public String getVoice () {
		return "silent";
	}
	
	public void eat() {
		System.out.println("Animal eating " + this.ration);
	}
	
	public void sleep() {
		System.out.println("Animal sleeping");
	}

	@Override
	public String toString() {
		return "Animals [ration=" + ration + ", color=" + color + ", weight=" + weight + "]";
	}
	
	
	
	
}
