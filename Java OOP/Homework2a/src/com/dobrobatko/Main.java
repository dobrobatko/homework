package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		Cat cat = new Cat("Viskas", "white", 5, "Kuzya");
		Dog dog = new Dog("Pedigree", "black", 25, "Samson");
		
		Veterinarian doctor = new Veterinarian("John");
		
		doctor.treatment(cat);
		doctor.treatment(dog);

	}

}
