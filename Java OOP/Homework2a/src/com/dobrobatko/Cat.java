package com.dobrobatko;

public class Cat extends Animals {
	private String name;

	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Cat() {
		super();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getVoice() {
		return "meow, meow";
	}
	
	@Override
	public void eat() {
		System.out.println("Cat eating " + getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println("Cat sleeping in a box.");
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", ration=" + getRation() + ", color=" + getColor() + ", weight="
				+ getWeight() + "]";
	}
	
	
}
