package com.dobrobatko;

public class Dog extends Animals {
	private String name;

	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Dog() {
		super();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getVoice() {
		return "woof, woof";
	}
	
	@Override
	public void eat() {
		System.out.println("Dog eating " + getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println("Dog sleeping in a booth.");
	}

	@Override
	public String toString() {
		return "Dog [name=" + name + ", ration=" + getRation() + ", color=" + getColor() + ", weight="
				+ getWeight() + "]";
	}
	
	
}
