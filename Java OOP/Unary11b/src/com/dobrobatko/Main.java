package com.dobrobatko;

import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		
		UnaryOperator<String> unOp = Main::onlyDigits;
		
		String in = "Hello 123 world 456 !";
		String out = unOp.apply(in);
		
		System.out.println(out);

	}
	
	public static String onlyDigits (String str) {
		char[] chArray = str.toCharArray();
		String result = "";
		for (char ch : chArray) {
			if(Character.isDigit(ch)) {
				result+=ch;
			}
		}
		return result;
	}

}
