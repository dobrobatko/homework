package com.dobrobatko;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Client implements Runnable {
	private Socket soc;
	private String answer;
	private Thread th;

	public Client(Socket soc, String answer) {
		super();
		this.soc = soc;
		this.answer = answer;
		th = new Thread(this);
		th.start();
	}

	@Override
	public void run() {
		try (InputStream is = soc.getInputStream();
				OutputStream os = soc.getOutputStream();
				PrintWriter pw = new PrintWriter(os)) {

			byte[] res = new byte[is.available()];
			is.read(res);
			String s = new String(res, StandardCharsets.UTF_8);
			System.out.println(s);

			String response = "HTTP/1.1 200 OK\r\n" + "Server: My_Server\r\n" + "Content-Type:text/html\r\n"
					+ "Content-Length: " + "\r\n" + "Connection: close\r\n\r\n";

			pw.print(response + answer);
			pw.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
