package com.dobrobatko;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

	public static void main(String[] args) {

		String answer;
		int connectionId = 0;

		try (ServerSocket soc = new ServerSocket(8181)) {
			for (;;) {
				connectionId++;
				answer = "";
				answer += "<html><head><title>My Server</title> <meta charset='utf-8'>"
						+ "<style>body{background-color:black; color:lime;}</style>" + "</head><body><h2>Server Info: "
						+ "</h2>";
				answer += "<div>" + SystemInfo.get() + "Connection ID: " + connectionId + "</div></body></html>";
				
				Socket clisoc = soc.accept();
				Client cli = new Client(clisoc, answer);
			}

		} catch (IOException e) {
			System.out.println("Error to server Socket Open!!!");
			e.printStackTrace();
		}
	}

}
