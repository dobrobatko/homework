package com.dobrobatko;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemInfo {
	
	public static String get () {
		StringBuilder sb = new StringBuilder("");
		Runtime run = Runtime.getRuntime();
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		
		
		sb.append("Processor cores: " + run.availableProcessors()).append("</br>");
		sb.append("Total memory: " + run.totalMemory()).append("</br>");
		sb.append("Free memory: " + run.freeMemory()).append("</br>");
		sb.append("Operation system: " + System.getProperty("os.name")).append("</br>");
		sb.append("System architecture: " + System.getProperty("os.arch")).append("</br>");
		sb.append("Server date: " + dateFormat.format(date)).append("</br>");
		sb.append("Server time: " + timeFormat.format(date)).append("</br>");
		
		return sb.toString();
	}
}
