package com.dobrobatko;

import java.util.TreeSet;

public class Group<T extends Student> {
	private String name;
	private TreeSet<T> students = new TreeSet<T>(new LastNameComparator()); 

	
	public Group(String name, TreeSet<T> students) {
		super();
		this.name = name;
		this.students = students;
	}

	public Group(String name) {
		super();
		this.name = name;
	}

	public Group() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void add(T student) {
		student.setGroup(name);
		students.add(student);
	}
	
	public void add(T[] array) {
		for (T student : array) {
			student.setGroup(name);
			students.add(student);
		}
	}

	public TreeSet<T> getStudents() {
		return students;
	}

	public Student findByLastName(String lastName) {
		for (Student student : students) {
			if (student.getLastName().equals(lastName)) {
				return student;
			}
		}
		return null;
	}

	private int countMembers(TreeSet<T> list) {
		int count = 0;
		for (Human person : list) {
			if (person != null) {
				count++;
			}
		}
		return count;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Group " + name + " consists of " + countMembers(students) + " members:");
		sb.append(System.lineSeparator());
		for (Student student : students) {
			if (student != null) {
				sb.append(student.toString());
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}



}
