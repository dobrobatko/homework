package com.dobrobatko;

public class FileProgress implements Runnable {
	private FileAction action;
	private double size;
	private double readed;
	private int percent;

	public FileProgress(FileAction action) {
		super();
		this.action = action;
	}

	@Override
	public void run() {
		size = action.getSize();
		for (; !action.isStop();) {

			readed = action.getReaded();
			percent = (int) (readed / (size / 100));
			System.out.println(percent + " %");

		}

	}

}
