package com.dobrobatko;

public class FileReader implements Runnable {
	private FileAction action;

	public FileReader(FileAction action) {
		super();
		this.action = action;
	}

	@Override
	public void run() {
		action.startStreams();
		for (; !action.isStop();) {
			action.readFile();
		}
		System.out.println("File " + action.getInput() + " has been read.");
	}

}
