package com.dobrobatko;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileAction {
	private String input;
	private String output;
	private State state = State.WAIT;
	private int bufferSize = 1024;
	private byte[] buffer = new byte[bufferSize];
	private boolean stop = false;
	int byteread = 0;
	private long readed = 0;
	private FileInputStream is;
	private FileOutputStream os;

	public void startStreams() {
		try {
			is = new FileInputStream(input);
			os = new FileOutputStream(output);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public FileAction(String input, String output) {
		super();
		this.input = input;
		this.output = output;
	}

	public FileAction() {
		super();
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getInput() {
		return input;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getOutput() {
		return output;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public byte[] getBuffer() {
		return buffer;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public long getReaded() {
		return readed;
	}

	public synchronized void readFile() {
		try {
			for (; (byteread = is.read(buffer)) > 0;) {
				readed += byteread;
				state = State.WRITE;
				notifyAll();
				for (; state != State.READ;) {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			stop = true;
			is.close();
			state = State.WRITE;
			notifyAll();
		} catch (IOException e) {
			e.printStackTrace();
			;
		}
	}

	public synchronized void writeFile() {
		try {
			for (; state != State.WRITE;) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (byteread > 0) {
				os.write(buffer, 0, byteread);
				state = State.READ;
				notifyAll();
			} else {
				os.close();
			}

		} catch (IOException e) {
			System.out.println("Can`t write file!");
		}
	}

	public long getSize() {
		File file = new File(input);
		return file.length();
	}

}
