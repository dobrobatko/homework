package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		FileAction action = new FileAction("1.dat", "2.dat");

		FileReader fr = new FileReader(action);
		FileProgress fp = new FileProgress(action);
		FileWriter fw = new FileWriter(action);

		Thread read = new Thread(fr);
		Thread progress = new Thread(fp);
		Thread write = new Thread(fw);

		read.start();
		progress.start();
		write.start();

	}

}
