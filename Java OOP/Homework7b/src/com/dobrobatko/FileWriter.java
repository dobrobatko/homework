package com.dobrobatko;

public class FileWriter implements Runnable {
	FileAction action;

	public FileWriter(FileAction action) {
		super();
		this.action = action;
	}

	@Override
	public void run() {
		for (; !action.isStop();) {
			action.writeFile();
		}
		System.out.println("File " + action.getOutput() + " has been written.");

	}

}
