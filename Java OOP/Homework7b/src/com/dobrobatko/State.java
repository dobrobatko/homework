package com.dobrobatko;

public enum State {
	READ, WRITE, WAIT
}
