package com.dobrobatko;

import java.util.function.IntSupplier;

public class Main {

	public static void main(String[] args) {

		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

		IntSupplier next = new NextItem(array);

		for (int i = 0; i < array.length; i++) {
			System.out.println(next.getAsInt());
		}

	}

}

class NextItem implements IntSupplier {
	private int[] array;
	private int index = -1;

	public NextItem(int[] array) {
		super();
		this.array = array;
	}

	@Override
	public int getAsInt() {
		index++;
		return array[index];
	}

}
