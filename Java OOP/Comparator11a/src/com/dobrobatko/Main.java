package com.dobrobatko;

import java.util.Arrays;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		
		Cat cat1 = new Cat("Apocalypse", 2);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 8);
		Cat cat4 = new Cat("Timka", 5);
		Cat cat5 = new Cat("Kuzia", 2);
		Cat cat6 = new Cat("Umka", 12);
		
		Cat[] cats = new Cat[] { cat1, cat2, cat3, cat4, cat5, cat6};

		Comparator<Cat> comp = (a,b) -> a.getName().length() - b.getName().length();
		
		Arrays.sort(cats, comp);
		
		for (Cat cat : cats) {
			System.out.println(cat);
		}
	}

}
