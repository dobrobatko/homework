package com.dobrobatko;

import java.util.Scanner;

public class Control {
	
	public static void action () {
		Dictionary dic = null;
		dic = FileOperation.readDicFromFile();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Type (t) for translate, (d) for adding new word, (s) show dictionary, (e) exit program:");
		String control = sc.nextLine();
		switch (control) {
		case "d":
			DictionaryOperation.addWord(dic);
			break;
		case "t":
			DictionaryOperation.translate(dic);
			break;
		case "s":
			System.out.println(dic);
			action();
			break;
		case "e":
			System.out.println("See you! Bye!");
			break;
		default:
			action();
			break;
		}
	}
}
