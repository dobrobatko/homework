package com.dobrobatko;

public class NoMatchException extends Exception {

	@Override
	public String getMessage() {
		return "No matches found.";
	}
}
