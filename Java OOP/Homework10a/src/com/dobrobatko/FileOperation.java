package com.dobrobatko;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileOperation {

	public static void writeDicToFile(Dictionary dic) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("dictionary.dat"))) {
			oos.writeObject(dic);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Dictionary readDicFromFile() {
		Dictionary dic = null;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("dictionary.dat"))) {
			dic = (Dictionary) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dic;
	}

	public static String readTextFromFile() {
		File file = new File("english.in");
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String str = "";
			for (; (str = br.readLine()) != null;) {
				line += str;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}
	
	public static void writeTextToFile (String text) {
		File file = new File("ukrainian.out");
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			bw.write(text);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
