package com.dobrobatko;

import java.util.Scanner;

public class DictionaryOperation {

	public static void addWord(Dictionary dic) {
		Scanner sc = new Scanner(System.in);
		String input;
		System.out.println("Input words pair (english word:ukrainian word). Type (0) for exit editor.");
		input = sc.nextLine();
		while (!input.equals("0")) {
			if (input.indexOf(':') >= 0 && input.indexOf(':') == input.lastIndexOf(':')) {
				String key = input.substring(0, input.indexOf(':'));
				String value = input.substring(input.indexOf(':') + 1);
				boolean flag = dic.addPair(key, value);
				if (flag) {
					System.out.println("Pair added.");
				} else {
					System.out.println("Error: This pair is already in the dictionary.");
				}

			} else {
				System.out.println("Error: Incorrect input. Try again.");
			}
			input = "";
			input = sc.nextLine();
		}
		FileOperation.writeDicToFile(dic);
		System.out.println("Dictionary updated successfully.");
		Control.action();
	}

	public static void translate(Dictionary dic) {
		String inputText = FileOperation.readTextFromFile().toLowerCase();
		String outputText = "";
		boolean flag = true;
		String[] words = inputText.split(" ");
		for (int i = 0; i < words.length; i++) {
			words[i] = removeChar(words[i], '.', ':', ';', ',', '!', '?');
			try {
				outputText += dic.getWord(words[i]) + " ";
			} catch (NoMatchException e) {
				flag = false;
				e.printStackTrace();
			}
		}
		if (flag) {
			FileOperation.writeTextToFile(outputText);
			System.out.println("Translated succesfully:");
			System.out.println("Input file => english.in");
			System.out.println("Output file => ukrainian.out");
		}
	}

	private static String removeChar(String word, char... chs) {
		for (char ch : chs) {
			int index = word.lastIndexOf(ch);
			if (index != -1) {
				StringBuffer sb = new StringBuffer(word);
				sb.delete(index, index + 1);
				return word = sb.toString();
			}
		}
		return word;
	}

}
