package com.dobrobatko;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Dictionary implements Serializable {
	private HashMap <String, String> dic = new HashMap<>();

	public Dictionary(HashMap<String, String> dic) {
		super();
		this.dic = dic;
	}

	public Dictionary() {
		super();
	}
	
	// Adding words pair to the dictionary
	public boolean addPair (String eng, String ukr) {
		if(dic.get(eng.toLowerCase()) == null) {
			dic.put(eng.toLowerCase(), ukr.toLowerCase());
			return true;
		}
		return false;
	}
	
	// Getting translated word from the dictionary
	public String getWord (String eng) throws NoMatchException {
		String word = dic.get(eng);
		if (word == null) {
			throw new NoMatchException();
		}
		return word;
	}

	@Override
	public String toString() {
		Set<Map.Entry<String, String>> dicSet = dic.entrySet(); 
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : dicSet) {
			sb.append(entry.getKey() + "\t" + entry.getValue());
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
	
	
	
	
}
