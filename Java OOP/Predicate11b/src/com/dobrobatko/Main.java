package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {

		Predicate<String> pr = Main::test;

		List<String> list = new ArrayList<>(List.of("Aaa", "Bbb", "Ccc", "Aaa", "Bbb", "Hhh", "Ttt", "Aaa", "Bbb"));

		list.removeIf(pr);
		
		System.out.println(list);

	}

	public static boolean test(String str) {
		char[] array = new char[] { 'h', 'a', 't' };
		for (char c : array) {
			if (str.toLowerCase().startsWith("" + c)) {
				return true;
			}
		}
		return false;

	}

}
