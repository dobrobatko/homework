package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		
		List<String> str = new ArrayList<>(List.of("I", "like", "Java"));
		
		Supplier<String> sup = new OnlyGenerals(str);
		
		for(int i = 0; i < 5; i++) {
			System.out.println(sup.get());
		}

	}

}

class OnlyGenerals implements Supplier<String> {
	private List<String> list = new ArrayList<>(); 
	private int index = -1;
	
	private Predicate<String> pr = a -> {
		if(a.charAt(0) >= 'A' && a.charAt(0) <= 'Z') {
			return true;
		}
		return false;
	};

	public OnlyGenerals(List<String> list) {
		super();
		this.list = list;
	}

	@Override
	public String get() {
		int size = list.size();
		if(index >= size -1) {
			return null;
		}
		index++;
		if(pr.test(list.get(index))) {
			return list.get(index);
		}
		return get();
	}
	
	
}
