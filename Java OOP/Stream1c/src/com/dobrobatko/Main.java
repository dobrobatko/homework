package com.dobrobatko;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {

	public static void main(String[] args) {
		File folder = new File(".");
		System.out.println("The file in this folder which has the maximum size is: " + maxSizeFile(folder));

	}
	
	public static String maxSizeFile (File folder) {
		File[] files = folder.listFiles();

		Optional<String> result = Arrays.stream(files)
				.max((a,b) -> (int)a.length() - (int)b.length())
				.map(a -> a.getName());
		return result.get();
	}				
}
