package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		List<String> listIn = new ArrayList<>(List.of("Hello", "Java", "the", "best", "language", "in", "the", "world!"));
		
		UnaryOperator<List<String>> unOp = Main::moreThenFive;
		
		List<String> listOut = unOp.apply(listIn);
		
		System.out.println(listOut);
		
	}
	
	public static List<String> moreThenFive (List<String> list) {
		List<String> result = new ArrayList<>();
		for (String str : list) {
			if(str.length() > 5) {
				result.add(str);
			}
		}
		return result;
	}

}
