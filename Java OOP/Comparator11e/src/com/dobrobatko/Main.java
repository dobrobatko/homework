package com.dobrobatko;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		File one = new File("a?---??.txt");
		File two = new File("a???.txt");
		File three = new File("a!?--!.txt");
		File four = new File("a-.txt");
		File five = new File("a??.txt");

		List<File> list = List.of(one, two, three, four, five);

		Comparator<Integer> comp = (a, b) -> Integer.compare(a, b);
		Comparator<File> resultComp = Comparator.comparing(Main::countSymbols, comp);

		File max = Collections.max(list, resultComp);
		System.out.println(max.getName());
		File min = Collections.min(list, resultComp);
		System.out.println(min.getName());
	}

	public static Integer countSymbols(File file) {
		char[] name = file.getName().toCharArray();
		Integer count = 0;
		char[] symbols = { ',', '.', '-', '?', '!' };
		for (int i = 0; i < name.length; i++) {
			for (int j = 0; j < symbols.length; j++) {
				if (name[i] == symbols[j]) {
					count++;
				}
			}
		}
		return count;
	}
}
