package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>(List.of("Aaa", "Bbb", "Ccc", "Aaa", "Bbb"));
		
		Predicate<String> pr = a -> a.startsWith("A");
		
		list.removeIf(pr);
		
		System.out.println(list);

	}

}
