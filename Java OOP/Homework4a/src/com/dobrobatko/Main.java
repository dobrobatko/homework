package com.dobrobatko;

public class Main {

	public static void main(String[] args) {

		Group f1 = new Group("f1");

		Student cl = new Student("Charle", "Leclerc", 24, "male", "Extreme driving", null);
		Student cs = new Student("Carlos", "Sainz", 28, "male", "Extreme driving", null);
		Student lh = new Student("Lewis", "Hamilton", 34, "male", "Extreme driving", null);
		Student mv = new Student("Max", "Verstappen", 24, "male", "Extreme driving", null);
		Student fa = new Student("Fernando", "Alonso", 34, "male", "Extreme driving", null);
		Student cp = new Student("Checo", "Perez", 32, "male", "Extreme driving", null);
		Student sv = new Student("Sebastian", "Vettel", 35, "male", "Extreme driving", null);

		Student[] freshmans = { cl, cs, lh, mv, fa, cp };
		Group[] groups = { f1 };

		try {
			f1.add(freshmans);
			f1.add(sv);

			Student beginner = SetStudent.apply();
			groups = acceptToGroup(groups, beginner);

		} catch (OverloadGroupException e) {
			e.printStackTrace();
		} catch (CancelInputException e) {
			e.printStackTrace();
		}

		f1.sortStudentsByLastName();

		for (Group group : groups) {
			System.out.println(group.toString());
		}

		Student sv1 = new Student();
		sv1 = sv1.fromCSVString(sv.toCSVString());
		System.out.println(sv1.toString());

	}

	public static Group[] acceptToGroup(Group[] array, Student newer) throws OverloadGroupException {
		for (int i = 0; i < array.length; i++) {
			if (array[i].getName().equals(newer.getGroup())) {
				array[i].add(newer);
				System.out.println("Student added to an existing group: " + array[i].getName());
				return array;
			}
		}
		Group firstCurse = new Group(newer.getGroup());
		firstCurse.add(newer);
		Group[] arr = new Group[array.length + 1];
		System.arraycopy(array, 0, arr, 0, array.length);
		arr[array.length] = firstCurse;
		System.out.println("Student added. New group " + firstCurse.getName() + " created");
		return arr;
	}
}
