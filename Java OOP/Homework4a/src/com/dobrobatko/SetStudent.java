package com.dobrobatko;

import javax.swing.JOptionPane;

public class SetStudent {
	// private static Student newer = new Student();
	public static Student apply() throws CancelInputException {
		Student newer = new Student();

		String name;
		String lastName;
		int age;
		String sex;
		String faculty;
		String group;

		for (;;) {
			name = String.valueOf(JOptionPane.showInputDialog("Input new student`s name:"));
			if (name.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(name);
				break;
			}
		}
		for (;;) {
			lastName = String.valueOf(JOptionPane.showInputDialog("Input new student`s last name:"));
			if (lastName.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(lastName);
				break;
			}
		}
		for (;;) {
			String ageString;
			ageString = String.valueOf(JOptionPane.showInputDialog("Input new student`s age:"));
			if (ageString.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(ageString);
				age = Integer.parseInt(ageString);
				break;
			}
		}
		for (;;) {
			sex = String.valueOf(JOptionPane.showInputDialog("Input new student`s sex:"));
			if (sex.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(sex);
				break;
			}
		}
		for (;;) {
			faculty = String.valueOf(JOptionPane.showInputDialog("Input new student`s faculty:"));
			if (faculty.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(faculty);
				break;
			}
		}
		for (;;) {
			group = String.valueOf(JOptionPane.showInputDialog("Input new student`s group:"));
			if (group.equals("")) {
				JOptionPane.showMessageDialog(null, "This field cannot be empty!");
			} else {
				isNull(group);
				break;
			}
		}
		newer.setName(name);
		newer.setLastName(lastName);
		newer.setAge(age);
		newer.setSex(sex);
		newer.setFaculty(faculty);
		newer.setGroup(group);
		
		return newer;
		
	}

	private static void isNull(String control) throws CancelInputException {
		if (control.equals("null")) {
			throw new CancelInputException();
		}
	}
	

}
