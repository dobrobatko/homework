package com.dobrobatko;

import java.util.Comparator;

public class LastNameComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		Human person1 = (Human) o1;
		Human person2 = (Human) o2;
		
		String lastName1 = person1.getLastName();
		String lastName2 = person2.getLastName();
		
		if(lastName1.compareTo(lastName2) > 0) {
			return 1;
		}
		if(lastName1.compareTo(lastName2) < 0) {
			return -1;
		}
		
		return 0;
	}

}
