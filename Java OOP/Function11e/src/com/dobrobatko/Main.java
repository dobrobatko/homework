package com.dobrobatko;

import java.util.Calendar;
import java.util.function.ToIntFunction;

public class Main {

	public static void main(String[] args) {
		ToIntFunction<Calendar> fun = a -> a.get(Calendar.YEAR);

		Calendar cl = Calendar.getInstance();

		System.out.println(fun.applyAsInt(cl));
	}

}
