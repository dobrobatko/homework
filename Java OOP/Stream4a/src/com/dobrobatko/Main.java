package com.dobrobatko;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		String text = "I learn Java and JS";
		String[] words = text.split(" ");

		List<String> vwlsWords = Arrays.stream(words)
				.filter(w -> countVowels(w) != 0)
				.sorted((a, b) -> countVowels(a) - countVowels(b))
				.toList();

		System.out.println(vwlsWords);

	}

	public static Integer countVowels(String text) {
		char[] vows = { 'a', 'e', 'i', 'o', 'u', 'y',
				'A', 'E', 'I', 'O', 'U', 'Y' };
		char[] letts = text.toCharArray();
		Integer counter = 0;
		for (char lett : letts) {
			for (char vow : vows) {
				if (lett == vow)
					counter++;
			}
		}
		return counter;
	}

}
