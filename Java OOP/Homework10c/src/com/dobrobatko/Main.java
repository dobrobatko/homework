package com.dobrobatko;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {

	public static void main(String[] args) {

		// Manual generate Object[] for example
		String[] array = { "a1", "a2", "a3", "a2", "a1", "b2", "a1", "a2", "a1", "b2", "b1" };

		Map<Object, Integer> number = count(array);
		Set<Object> keys = number.keySet();
		for (Object key : keys) {
			System.out.println(key + " = " + number.get(key));
		}

	}

	public static Map<Object, Integer> count(Object[] array) {
		Map<Object, Integer> number = new HashMap<>();
		for (Object i : array) {
			Integer element = number.get(i);
			if (element != null) {
				number.put(i, element + 1);
			} else {
				number.put(i, 1);
			}
		}
		return number;
	}

}
