package com.dobrobatko;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.BiFunction;

public class Main {

	public static void main(String[] args) {

		BiFunction<String, String, String[]> biFun = Main::counterRepeat;

		String a = "Hello Java";
		String b = "Hello Python and Java";
		System.out.println(Arrays.toString(biFun.apply(a, b)));

	}

	public static String[] counterRepeat(String strOne, String strTwo) {
		String[] arrOne = strOne.split(" ");
		String[] arrTwo = strTwo.split(" ");
		ArrayList<String> list = new ArrayList<>();
		for (int i = 0; i < arrOne.length; i++) {
			for (int j = 0; j < arrTwo.length; j++) {
				if (arrOne[i].equals(arrTwo[j]))
					list.add(arrTwo[j]);
			}
		}
		String[] array = new String[list.size()];
		array = list.toArray(array);
		return array;
	}

}
