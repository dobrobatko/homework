package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		Thread[] ths = new Thread[100];
		
		for (int i = 0; i < ths.length; i++) {
			ths[i] = new Thread(new FactorialThread(i));
			ths[i].start();
		}
		
		System.out.println(Thread.currentThread().getName() + " is stopped.");
	}

}
