package com.dobrobatko;

import java.math.BigInteger;

public class FactorialThread implements Runnable{
	private int number;

	public FactorialThread(int number) {
		super();
		this.number = number;
	}

	public int getNumber() {
		return number;
	}
	
	private BigInteger calcFactorial (int number) {
		BigInteger fact = new BigInteger("1");
		for(int i=2; i<=number; i++) {
			fact = fact.multiply(new BigInteger(""+i));
		}
		return fact;	
	}

	@Override
	public void run() {
		Thread th = Thread.currentThread();
		for (int i = 1; i <= this.number; i++) {
			System.out.println(th.getName()+" "+i+"!= "+calcFactorial(i));
		}
	}
	

}
