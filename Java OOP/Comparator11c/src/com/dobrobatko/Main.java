package com.dobrobatko;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>(List.of(62, 2000, 306, 55));

		Comparator<Integer> comp = (a, b) -> {
			return Integer.compare(sumFirstLast(a), sumFirstLast(b));
		};

		Collections.sort(numbers, comp);

		System.out.println(numbers);

	}

	public static Integer sumFirstLast(Integer number) {
		Integer first = 0;
		for (int i = 10;; i = i * 10) {
			Integer temp = number / i;
			if (temp != 0) {
				first = temp;
			} else {
				break;
			}
		}
		Integer second = number % 10;
		return first + second;
	}

//	public static Integer sumFirstLast(Integer number) {
//		char[] cips = String.valueOf(number).toCharArray();
//		int first = Integer.valueOf(cips[0]+"");
//		int second = Integer.valueOf(cips[cips.length -1]+"");
//		return first + second;
//	}

}
