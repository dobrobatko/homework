package com.dobrobatko;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class Main {

	public static void main(String[] args) {

		String text = "I learn Java and JS and Java";

		Collector<String, Map<String, Integer>, Map<String, Integer>> collector = new OnlyVowelsContaining();

		Map<String, Integer> wordsMap = (HashMap<String, Integer>) Arrays.stream(text.split(" "))
				.distinct()
				.collect(collector);

		for (Map.Entry<String, Integer> entry : wordsMap.entrySet()) {
			String key = entry.getKey();
			Integer val = entry.getValue();
			System.out.println("Word " + "\"" + key + "\"" + " has " + val + " vowels.");
		}

	}

}

class OnlyVowelsContaining implements Collector<String, Map<String, Integer>, Map<String, Integer>> {

	@Override
	public Supplier<Map<String, Integer>> supplier() {
		return HashMap::new;
	}

	@Override
	public BiConsumer<Map<String, Integer>, String> accumulator() {
		return (map1, element) -> {
			if (countVowels(element) != 0) {
				map1.put(element, countVowels(element));
			}
		};
	}

	@Override
	public BinaryOperator<Map<String, Integer>> combiner() {
		return (map1, map2) -> {
			Map<String, Integer> comb = new HashMap<>(map1);
			map2.forEach((k, v) -> comb.put(k, v));
			return comb;
		};
	}

	@Override
	public Function<Map<String, Integer>, Map<String, Integer>> finisher() {
		Function.identity();
		return null;
	}

	@Override
	public Set<Characteristics> characteristics() {
		return Set.of(Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED);
	}

	public static Integer countVowels(String text) {
		char[] vows = { 'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y' };
		char[] letts = text.toCharArray();
		Integer counter = 0;
		for (char lett : letts) {
			for (char vow : vows) {
				if (lett == vow)
					counter++;
			}
		}
		return counter;
	}

}
