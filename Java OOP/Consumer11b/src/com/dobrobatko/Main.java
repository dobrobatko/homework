package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();
	
		Consumer<String> con = (a) -> {
			char[] chArray = a.toCharArray();
			for (char ch : chArray) {
				if(Character.isDigit(ch)) {
					list.add(a);
					break;
				}
			}
		};
		
		con.accept("Hello word!");
		con.accept("Hello Java 8!");
		con.accept("Java number 1!");
				
		System.out.println(list);
	}
	

}
