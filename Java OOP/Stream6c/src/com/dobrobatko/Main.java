package com.dobrobatko;

import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		
		List<Integer> list = List.of(1,2,3,4,5,6,7,8,10,9);
		
		BinaryOperator<Integer> biOp = (a,b) -> a > b ? a : b;
		Optional<Integer> max = list.stream()
				.reduce(biOp);
		
		System.out.println(max.get());

	}

}
