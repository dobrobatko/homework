package com.dobrobatko;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		File folder = new File("example");

		Map<String, List<Integer>> sizes = Map.of("Small (0-100 KB)", List.of(0, 100), "Medium (101-500 KB)",
				List.of(101, 500), "Big (501-1000 KB)", List.of(501, 1000));

		Function<File, String> classifier = file -> {
			for (String type : sizes.keySet()) {
				Integer size = (int) (file.length() / 1024);
				List<Integer> limits = new ArrayList<>(sizes.get(type));
				if (size >= limits.get(0) && size <= limits.get(1)) {
					return type;
				}
			}
			return "out of borders";
		};

		Map<String, List<File>> sorted = Arrays.stream(folder.listFiles()).collect(Collectors.groupingBy(classifier));

		TreeMap<String, List<File>> out = new TreeMap<>(sorted);

		out.forEach((size, fileList) -> {
			System.out.println(size);
			fileList.forEach(file -> {
				System.out.println("\t" + file + "\t" + file.length()/1024 + " KB");
			});
		});
	}

}
