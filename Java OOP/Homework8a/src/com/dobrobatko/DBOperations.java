package com.dobrobatko;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DBOperations {

	public DBOperations() {
		super();
	}
	
	public static void writeGroupToDb (Group group, String path) {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
			oos.writeObject(group);
		}catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Group " + group.getName() + " was written successfully in file " + path);
	}
	
	public static Group readGroupFromDb (String path) {
		Group group = null;
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
			group = (Group) ois.readObject();
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Group " + group.getName() + " was read successfully from file " + path);
		return group;
	}
	
}
