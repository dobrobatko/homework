package com.dobrobatko;

public class Main {

	public static void main(String[] args) {

		Group f1 = new Group("f1");

		Student cl = new Student("Charle", "Leclerc", 24, "male", "Extreme driving", null);
		Student cs = new Student("Carlos", "Sainz", 28, "male", "Extreme driving", null);
		Student lh = new Student("Lewis", "Hamilton", 34, "male", "Extreme driving", null);
		Student mv = new Student("Max", "Verstappen", 24, "male", "Extreme driving", null);
		Student fa = new Student("Fernando", "Alonso", 34, "male", "Extreme driving", null);
		Student cp = new Student("Checo", "Perez", 32, "male", "Extreme driving", null);
		Student sv = new Student("Sebastian", "Vettel", 35, "male", "Extreme driving", null);

		Student[] freshmans = { cl, cs, lh, mv, fa, cp, sv };

		try {
		f1.add(freshmans);
		System.out.println(f1.toString());
		} catch (OverloadGroupException e) {
			e.printStackTrace();
		}
		
		DBOperations.writeGroupToDb(f1, "f1.dat");
		
		
		Group f2 = null;
		f2 = DBOperations.readGroupFromDb("f1.dat");
		System.out.println(f2.toString());
	}
}
