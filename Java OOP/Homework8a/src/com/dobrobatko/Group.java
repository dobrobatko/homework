package com.dobrobatko;

import java.io.Serializable;
import java.util.Arrays;

public class Group implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Student[] students = new Student[10];

	public Group(String name, Student[] students) {
		super();
		this.name = name;
		this.students = students;
	}

	public Group(String name) {
		super();
		this.name = name;
	}

	public Group() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student[] getStudents() {
		return students;
	}

	public void add(Student student) throws OverloadGroupException {
		boolean flag = false;
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				students[i].setGroup(name);
				flag = true;
				break;
			}
		}
		if (flag != true) {
			throw new OverloadGroupException();
		}
	}

	public void add(Student[] fewStudents) throws OverloadGroupException {
		if (fewStudents.length > students.length) {
			throw new OverloadGroupException();
		}
		for (int i = 0; i < fewStudents.length; i++) {
			add(fewStudents[i]);
		}
	}

	@Override
	public String toString() {
		return "Group [name=" + name + ", students=" + Arrays.toString(students) + "]";
	}


}
