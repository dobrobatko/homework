package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		File folders = new File("folders.txt");
		Stream<File> stream = null;

		try {
			stream = Files.lines(folders.toPath())
					.distinct()
					.map(f -> new File(f))
					.filter(f -> f.isDirectory())
					.filter(d -> txtFilesCounter(d) > 3);
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<File> list = stream.toList();
		System.out.println(list);

	}

	public static Integer txtFilesCounter(File dir) {

		File[] arr = dir.listFiles((new CustomFileFilter("txt")));

		return arr.length;
	}
}
