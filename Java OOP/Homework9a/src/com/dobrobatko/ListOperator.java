package com.dobrobatko;

import java.util.ArrayList;
import java.util.Iterator;

public class ListOperator<T> {
	private ArrayList<T> list = new ArrayList<>();

	public ListOperator() {
		super();
	}

	public ArrayList<T> getList() {
		return list;
	}

	public void generateListInteger(int counter) {
		int past;
		for (int i = 0; i < counter; i++) {
			past = i * 10 + 10;
			list.add((T) Integer.valueOf(past));
		}
	}

	public void cutList() {
		list.remove(0);
		list.remove(0);
		list.remove(list.size() - 1);
	}

	@Override
	public String toString() {
		String out = "ListOperator list =>";
		Iterator<T> itr = list.iterator();
		for (; itr.hasNext();) {
			out += " " + itr.next();
		}
		return out;
	}

}
