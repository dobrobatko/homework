package com.dobrobatko;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {

		String text = "Hello interesting Java world";

		String[] strArray = text.split(" ");

		BiFunction<Integer, String, Integer> biFunc = (a, b) -> a + b.length();
		BinaryOperator<Integer> biOp = (a, b) -> a + b;
		
		Integer letterSum = Arrays.stream(strArray)
				.filter(s -> s.length() > 4)
				.reduce(0, biFunc, biOp);
		
		System.out.println("We have " + letterSum + " letters.");

	}

}
