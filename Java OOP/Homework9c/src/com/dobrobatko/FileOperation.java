package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileOperation {

	public FileOperation() {
		super();
	}

	public static String getTextFromFile(String path) {
		File file = new File(path);
		StringBuilder sb = new StringBuilder();
		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNextLine();) {
				sb.append(sc.nextLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString().toLowerCase();

	}
}
