package com.dobrobatko;

public class Letter {
	private char letter;
	private long counter;

	public Letter(char letter) {
		super();
		this.letter = letter;
		this.counter = 0;
	}

	public Letter() {
		super();
		this.counter = 0;
	}

	public char getLetter() {
		return letter;
	}

	public void setLetter(char letter) {
		this.letter = letter;
	}

	public long getCounter() {
		return counter;
	}

	public void countLetter(String text) {
		char[] array = text.toCharArray();
		for (char ch : array) {
			if (ch == letter) {
				counter++;
			}
		}
	}

	@Override
	public String toString() {
		return "Letter [letter=" + letter + ", counter=" + counter + "]";
	}

}
