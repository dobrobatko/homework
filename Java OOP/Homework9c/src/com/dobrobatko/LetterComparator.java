package com.dobrobatko;

import java.util.Comparator;

public class LetterComparator implements Comparator<Letter> {

	@Override
	public int compare(Letter o1, Letter o2) {
		if (o1.getCounter() > o2.getCounter()) {
			return -1;
		} else if (o1.getCounter() < o2.getCounter()) {
			return 1;
		} else
			return 0;
	}

}
