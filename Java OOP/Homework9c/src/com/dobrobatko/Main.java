package com.dobrobatko;

import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {

		String text = FileOperation.getTextFromFile("text.txt");
		ArrayList<Letter> list = new ArrayList<Letter>();
		
		for(char i = 'a'; i <= 'z'; i++) {
			Letter temp = new Letter(i);
			temp.countLetter(text);
			list.add(temp);

		}
		list.sort(new LetterComparator());
		for (Letter letter : list) {
			System.out.print(letter.getLetter() + "\t");
			System.out.println(letter.getCounter());
		}

	}

}
