package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		File urls = new File("urls.txt");
		List<String> activeURLs = null;

		try {
			List<String> listOfUrls = Files.lines(urls.toPath())
					.filter(Main::isActive)
					.toList();
			activeURLs = listOfUrls;
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String link : activeURLs) {
			System.out.println(link);
		}

	}

	public static boolean isActive(String link) {
		try {
			URL url = new URL(link);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			return connection.getResponseCode() == 200;
		} catch (UnknownHostException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}

}
