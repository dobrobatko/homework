package com.dobrobatko;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Integer> numbers = List.of(7, 19, 28, 346, 117, 2453, 1);
		
		numbers.stream()
			.filter(n -> n > 10)
			.sorted((a,b) -> (a%10) - (b%10))
			.forEach(System.out::println);

	}

}
