package com.dobrobatko;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 18, 481);

		Collector<Integer, ?, Map<String, List<Integer>>> downstream = Collectors
				.groupingBy(a -> (a % 2 == 0) ? "even" : "odd");

		Function<Map<String, List<Integer>>, List<String>> finisher = a -> {
			List<String> result = new ArrayList<>();
			for (Map.Entry<String, List<Integer>> entry : a.entrySet()) {
				String temp = "";
				List<Integer> val = entry.getValue();
				for (Integer integer : val) {
					temp += integer + ";";
				}
				temp = temp.substring(0, temp.length() - 1);
				result.add(temp);
			}
			return result;
		};

		List<String> strings = list.stream().collect(Collectors.collectingAndThen(downstream, finisher));

		Arrays.stream(strings.toArray())
		.forEach(System.out::println);
	}

}
