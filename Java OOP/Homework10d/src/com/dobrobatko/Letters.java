package com.dobrobatko;

import java.util.HashMap;
import java.util.Map;

public class Letters {

	public static Map<String, String[]> getletters() {
		Map<String, String[]> letters = new HashMap<>();

		String[] a = new String[6];
		a[0] = " ______    ";
		a[1] = "/\\  __ \\   ";
		a[2] = "\\ \\  __ \\  ";
		a[3] = " \\ \\_\\ \\_\\ ";
		a[4] = "  \\/_/\\/_/ ";
		a[5] = "           ";
		
		String[] b = new String[6];
		b[0] = " ______    ";
		b[1] = "/\\  == \\   ";
		b[2] = "\\ \\  __<   ";
		b[3] = " \\ \\_____\\ ";
		b[4] = "  \\/_____/ ";
		b[5] = "           ";
		
		String[] c = new String[6];
		c[0] = " ______    ";
		c[1] = "/\\  ___\\   ";
		c[2] = "\\ \\ \\____  ";
		c[3] = " \\ \\_____\\ ";
		c[4] = "  \\/_____/ ";
		c[5] = "           ";
		
		String[] d = new String[6];
		d[0] = " _____    ";
		d[1] = "/\\  __-.  ";
		d[2] = "\\ \\ \\/\\ \\ ";
		d[3] = " \\ \\____- ";
		d[4] = "  \\/____/ ";
		d[5] = "          ";
		
		String[] e = new String[6];
		e[0] = " ______    ";
		e[1] = "/\\  ___\\   ";
		e[2] = "\\ \\  __\\   ";
		e[3] = " \\ \\_____\\ ";
		e[4] = "  \\/_____/ ";
		e[5] = "           ";
		
		String[] f = new String[6];
		f[0] = " ______  ";
		f[1] = "/\\  ___\\ ";
		f[2] = "\\ \\  __\\ ";
		f[3] = " \\ \\_\\   ";
		f[4] = "  \\/_/   ";
		f[5] = "         ";
		
		String[] g = new String[6];
		g[0] = " ______    ";
		g[1] = "/\\  ___\\   ";
		g[2] = "\\ \\ \\__ \\  ";
		g[3] = " \\ \\_____\\ ";
		g[4] = "  \\/_____/ ";
		g[5] = "           ";
		
		String[] h = new String[6];
		h[0] = " __  __    ";
		h[1] = "/\\ \\_\\ \\   ";
		h[2] = "\\ \\  __ \\  ";
		h[3] = " \\ \\_\\ \\_\\ ";
		h[4] = "  \\/_/\\/_/ ";
		h[5] = "           ";
		
		String[] i = new String[6];
		i[0] = " __    ";
		i[1] = "/\\ \\   ";
		i[2] = "\\ \\ \\  ";
		i[3] = " \\ \\_\\ ";
		i[4] = "  \\/_/ ";
		i[5] = "       ";
		
		String[] j = new String[6];
		j[0] = "   __    ";
		j[1] = "  /\\ \\   ";
		j[2] = " _\\_\\ \\  ";
		j[3] = "/\\_____\\ ";
		j[4] = "\\/_____/ ";
		j[5] = "         ";
		
		String[] k = new String[6];
		k[0] = " __  __    ";
		k[1] = "/\\ \\/ /    ";
		k[2] = "\\ \\  _\"-.  ";
		k[3] = " \\ \\_\\ \\_\\ ";
		k[4] = "  \\/_/\\/_/ ";
		k[5] = "           ";
		
		String[] l = new String[6];
		l[0] = " __        ";
		l[1] = "/\\ \\       ";
		l[2] = "\\ \\ \\____  ";
		l[3] = " \\ \\_____\\ ";
		l[4] = "  \\/_____/ ";
		l[5] = "           ";
		
		String[] m = new String[6];
		m[0] = " __    __    ";
		m[1] = "/\\ \"-./  \\   ";
		m[2] = "\\ \\ \\-./\\ \\  ";
		m[3] = " \\ \\_\\ \\ \\_\\ ";
		m[4] = "  \\/_/  \\/_/ ";
		m[5] = "             ";
		
		String[] n = new String[6];
		n[0] = " __   __    ";
		n[1] = "/\\ \"-.\\ \\   ";
		n[2] = "\\ \\ \\-.  \\  ";
		n[3] = " \\ \\_\\\\\"\\_\\ ";
		n[4] = "  \\/_/ \\/_/ ";
		n[5] = "            ";
		
		String[] o = new String[6];
		o[0] = " ______    ";
		o[1] = "/\\  __ \\   ";
		o[2] = "\\ \\ \\/\\ \\  ";
		o[3] = " \\ \\_____\\ ";
		o[4] = "  \\/_____/ ";
		o[5] = "           ";
		
		String[] p = new String[6];
		p[0] = " ______  ";
		p[1] = "/\\  == \\ ";
		p[2] = "\\ \\  _-/ ";
		p[3] = " \\ \\_\\   ";
		p[4] = "  \\/_/   ";
		p[5] = "         ";
		
		String[] q = new String[6];
		q[0] = " ______    ";
		q[1] = "/\\  __ \\   ";
		q[2] = "\\ \\ \\/\\_\\  ";
		q[3] = " \\ \\___\\_\\ ";
		q[4] = "  \\/___/_/ ";
		q[5] = "           ";
		
		String[] r = new String[6];
		r[0] = " ______    ";
		r[1] = "/\\  == \\   ";
		r[2] = "\\ \\  __<   ";
		r[3] = " \\ \\_\\ \\_\\ ";
		r[4] = "  \\/_/ /_/ ";
		r[5] = "           ";
		
		String[] s = new String[6];
		s[0] = " ______    ";
		s[1] = "/\\  ___\\   ";
		s[2] = "\\ \\___  \\  ";
		s[3] = " \\/\\_____\\ ";
		s[4] = "  \\/_____/ ";
		s[5] = "           ";
		
		String[] t = new String[6];
		t[0] = " ______  ";
		t[1] = "/\\__  _\\ ";
		t[2] = "\\/_/\\ \\/ ";
		t[3] = "   \\ \\_\\ ";
		t[4] = "    \\/_/ ";
		t[5] = "         ";
		
		String[] u = new String[6];
		u[0] = " __  __    ";
		u[1] = "/\\ \\/\\ \\   ";
		u[2] = "\\ \\ \\_\\ \\  ";
		u[3] = " \\ \\_____\\ ";
		u[4] = "  \\/_____/ ";
		u[5] = "           ";
		
		String[] v = new String[6];
		v[0] = " __   __  ";
		v[1] = "/\\ \\ / /  ";
		v[2] = "\\ \\ \\'/   ";
		v[3] = " \\ \\__|   ";
		v[4] = "  \\/_/    ";
		v[5] = "          ";
		
		String[] w = new String[6];
		w[0] = " __     __    ";
		w[1] = "/\\ \\  _ \\ \\   ";
		w[2] = "\\ \\ \\/ \".\\ \\  ";
		w[3] = " \\ \\__/\".~\\_\\ ";
		w[4] = "  \\/_/   \\/_/ ";
		w[5] = "              ";
		
		String[] x = new String[6];
		x[0] = " __  __    ";
		x[1] = "/\\_\\_\\_\\   ";
		x[2] = "\\/_/\\_\\/_  ";
		x[3] = "  /\\_\\/\\_\\ ";
		x[4] = "  \\/_/\\/_/ ";
		x[5] = "           ";
		
		String[] y = new String[6];
		y[0] = " __  __    ";
		y[1] = "/\\ \\_\\ \\   ";
		y[2] = "\\ \\____ \\  ";
		y[3] = " \\/\\_____\\ ";
		y[4] = "  \\/_____/ ";
		y[5] = "           ";
		
		String[] z = new String[6];
		z[0] = " ______    ";
		z[1] = "/\\___  \\   ";
		z[2] = "\\/_/  /__  ";
		z[3] = "  /\\_____\\ ";
		z[4] = "  \\/_____/ ";
		z[5] = "           ";
		
		letters.put("a", a);
		letters.put("b", b);
		letters.put("c", c);
		letters.put("d", d);
		letters.put("e", e);
		letters.put("f", f);
		letters.put("g", g);
		letters.put("h", h);
		letters.put("i", i);
		letters.put("j", j);
		letters.put("k", k);
		letters.put("l", l);
		letters.put("m", m);
		letters.put("n", n);
		letters.put("o", o);
		letters.put("p", p);
		letters.put("q", q);
		letters.put("r", r);
		letters.put("s", s);
		letters.put("t", t);
		letters.put("u", u);
		letters.put("v", v);
		letters.put("w", w);
		letters.put("x", x);
		letters.put("y", y);
		letters.put("z", z);
		
		return letters;
	}
	
	static void printASCII(String text) {
		char[] charArray = text.toLowerCase().toCharArray();
		Map <String, String[]> letters = getletters();
		for(int i=0; i<6; i++) {
			for(int j=0; j<charArray.length; j++) {
				String[] letter = letters.get(""+charArray[j]);
				System.out.print(letter[i]);
			}
			System.out.println();
		}
	}
}
