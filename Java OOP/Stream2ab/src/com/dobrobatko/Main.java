package com.dobrobatko;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		// Stream API part 2 Task 1
		String strOne = "Java was originally developed by James Gosling at Sun Microsystems";
		String[] words = strOne.split(" ");
		String result = "";

		List<String> temp = Arrays.stream(words)
				.filter(w -> w.length() <= 5)
				.collect(Collectors.toList());

		for (String word : temp) {
			result += word + " ";
		}
		
		System.out.println(result);

		// Stream API part 2 Task 2
		String strTwo = "On May 8, 2007, Sun finished the process";
		result = "";
		String[] chs = strTwo.split("");
		List<String> tempCh = Arrays.stream(chs)
				.filter(ch -> isEngLetter(ch.charAt(0)))
				.collect(Collectors.toList());

		for (String ch : tempCh) {
			result += ch;
		}

		System.out.println(result);
	}

	public static boolean isEngLetter(char ch) {
		if (ch >= 'a' && ch <= 'z')
			return true;
		if (ch >= 'A' && ch <= 'Z')
			return true;
		return false;
	}

}
