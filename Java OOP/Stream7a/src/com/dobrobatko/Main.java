package com.dobrobatko;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class Main {

	public static void main(String[] args) {
		Collector<Integer, Set<Integer>, Set<Integer>> collector = new ToSet<Integer>();

		List<Integer> list = List.of(-1, -3, -3, 0, 7, 5, 1, 5, 5, 7, 10);
		
		Set<Integer> upperNoRepeat = list.stream()
				.filter(a -> a > 0)
				.collect(collector);
		
		System.out.println(upperNoRepeat);

	}

}

class ToSet<E> implements Collector<E, Set<E>, Set<E>> {

	@Override
	public Supplier<Set<E>> supplier() {
		return HashSet<E>::new;
	}

	@Override
	public BiConsumer<Set<E>, E> accumulator() {
		return (a, b) -> a.add(b);
	}

	@Override
	public BinaryOperator<Set<E>> combiner() {
		return (a, b) -> {
			Set<E> comb = new HashSet<>();
			comb.addAll(a);
			comb.addAll(b);
			return comb;
		};
	}

	@Override
	public Function<Set<E>, Set<E>> finisher() {
		return Function.identity();
	}

	@Override
	public Set<Characteristics> characteristics() {
		return Set.of();
	}

}
