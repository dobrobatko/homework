package com.dobrobatko;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		List<Integer> numsOne = List.of(1, 3, 5, 7, 9, 9);
		List<Integer> numsTwo = List.of(2, 4, 5, 5, 6, 8, 9);

		BinaryOperator<List<Integer>> biOp = Main::sameNumbers;

		List<Integer> theSame = biOp.apply(numsOne, numsTwo);

		System.out.println(theSame);

	}

	public static List<Integer> sameNumbers(List<Integer> list1, List<Integer> list2) {
		TreeSet<Integer> ts = new TreeSet<>();
		for (Integer ls1 : list1) {
			for (Integer ls2 : list2) {
				if (ls1 == ls2) {
					ts.add(ls1);
					break;
				}
			}
		}
		List<Integer> result = new ArrayList<>(ts);
		return result;
	}

}
