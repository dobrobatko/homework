package com.dobrobatko;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		String strOne = "Hello world";
		String strTwo = "Java forever";

		BinaryOperator<String> biOp = Main::biggestWord;

		System.out.println(biOp.apply(strOne, strTwo));

	}

	public static String biggestWord(String str1, String str2) {
		String common = str1 + " " + str2;
		String[] arr = common.split(" ");
		Comparator<String> comp = (a, b) -> a.length() - b.length();
		Arrays.sort(arr, comp);
		return arr[arr.length - 1];
	}

}
