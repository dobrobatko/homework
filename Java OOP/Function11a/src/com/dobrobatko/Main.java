package com.dobrobatko;

import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {
		Function<Integer[], Integer> fun = Main::counterSimple;

		// Integer[] array = {5,6,7,8,9,10};
		Integer[] array = generateArray(100, 100);
		System.out.println(fun.apply(array));
		System.out.println(Arrays.toString(array));

	}

	public static Integer[] generateArray(int size, int max) {
		Random rn = new Random();
		Integer[] array = new Integer[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = rn.nextInt(max + 1);
		}
		return array;
	}

	public static Integer counterSimple(Integer[] array) {
		Predicate<Integer> simple = a -> {
			for (int i = 2; i < a; i++) {
				if (a % i == 0)
					return false;
			}
			return true;
		};
		int counter = 0;
		for (Integer num : array) {
			if (simple.test(num))
				counter++;
		}
		return counter;
	}

}
