package com.dobrobatko;

import java.util.function.Function;

public class Main {

	public static void main(String[] args) {

		Function<String, Integer> fun = Main::countVowels;

		System.out.println(fun.apply("Hello world"));

	}

	public static Integer countVowels(String text) {
		char[] vows = { 'a', 'e', 'i', 'o', 'u', 'y' };
		char[] letts = text.toCharArray();
		Integer counter = 0;
		for (char lett : letts) {
			for (char vow : vows) {
				if (lett == vow)
					counter++;
			}
		}
		return counter;
	}

}
