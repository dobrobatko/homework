package com.dobrobatko;

public class OverloadStackException extends Exception {

	@Override
	public String getMessage() {
		return "Error: Stack overloaded! Last object has not been added.";
	}
}
