package com.dobrobatko;

public class Triangle {
	private String name;
	private double sideA;
	private double sideB;
	private double sideC;
	
	public Triangle(String name, double sideA, double sideB, double sideC) {
		super();
		this.name = name;
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
	}

	public Triangle() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSideA() {
		return sideA;
	}

	public void setSideA(double sideA) {
		this.sideA = sideA;
	}

	public double getSideB() {
		return sideB;
	}

	public void setSideB(double sideB) {
		this.sideB = sideB;
	}

	public double getSideC() {
		return sideC;
	}

	public void setSideC(double sideC) {
		this.sideC = sideC;
	}

	@Override
	public String toString() {
		return "Triangle [name=" + name + ", sideA=" + sideA + ", sideB=" + sideB + ", sideC=" + sideC + "]";
	}
	
	
	
	
}


