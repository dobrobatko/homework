package com.dobrobatko;

import java.util.Arrays;

public class Stack {
	private Object[] stack = new Object[10];

	public Stack(Object[] stack) {
		super();
		this.stack = stack;
	}

	public Stack() {
		super();
	}

	public int getAvailableSpace() {
		int counter = 0;
		for (Object object : stack) {
			if (object == null) {
				counter++;
			}
		}
		return counter;
	}

	public void push(Object obj) throws OverloadStackException {
		boolean flag = !true;
		if (BlackList.isDesirable(obj.getClass())) {
			for (int i = 0; i < stack.length; i++) {
				if (stack[i] == null) {
					stack[i] = obj;
					flag = true;
					System.out.println("Object " + obj + " added to Stack.");
					break;
				}
			}
			if (!flag) {
				throw new OverloadStackException();
			}
		} else {
			System.out.println("You trying place unwanted object in the stack!");
		}
	}

	public Object pop() {
		Object temp = null;
		for (int i = stack.length - 1; i < stack.length; i--) {
			if (stack[i] != null) {
				temp = stack[i];
				stack[i] = null;
				break;
			}
		}
		return temp;
	}

	public Object copy() {
		for (int i = stack.length - 1; i < stack.length; i--) {
			if (stack[i] != null) {
				return stack[i];
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "Stack [stack=" + Arrays.toString(stack) + "]";
	}

}
