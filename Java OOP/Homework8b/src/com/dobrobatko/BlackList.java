package com.dobrobatko;

public class BlackList {
	private static String[] unwanted;

	public BlackList() {
		super();
	}
	
	public static void setList (String[] unwanted) {
		BlackList.unwanted = unwanted;
	}
	
	public static String getListString() {
		StringBuilder sb = new StringBuilder("");
		for (String string : unwanted) {
			sb.append(string);
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
	
	public static String[] getListArray() {
		String[] tempArray = new String[unwanted.length];
		System.arraycopy(unwanted, 0, tempArray, 0, unwanted.length);
		return tempArray;
	}
	
	public static boolean isDesirable (Class cl) {
		String className = cl.getName();
		for (String string : unwanted) {
			if(className.equals(string)) {
				return false;
			}
		}
		return true;	
	}
	
}
