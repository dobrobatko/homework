package com.dobrobatko;

public class Main {

	public static void main(String[] args) {
		
		String[] blackClasses = {
				"java.lang.Double",
				"java.lang.Integer",
				"java.lang.String"
		};	
		BlackList.setList(blackClasses);
		
		Stack st = new Stack();
		System.out.println(st.getAvailableSpace() + " places left in stack.");
		
		Triangle trOne = new Triangle("One", 4, 5, 3);
		Triangle trTwo = new Triangle("Two", 10, 5, 7);
		Triangle trThree = new Triangle("Three", 20, 15, 10);
		Integer a = 5;
		
		try {
			st.push(trOne);
			st.push(trTwo);
			st.push(trThree);
			st.push(a);
		} catch (OverloadStackException e) {
			e.printStackTrace();
		}
		
		System.out.println(st.getAvailableSpace() + " places left in stack.");
		System.out.println(st);
		
		Object trFour = st.pop();
		Object trFive = st.copy();
		
		System.out.println(st.getAvailableSpace() + " places left in stack.");
		System.out.println(st);
		System.out.println(trFour);
		System.out.println(trFive);
		

	}

}
