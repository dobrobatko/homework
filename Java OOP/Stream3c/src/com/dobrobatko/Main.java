package com.dobrobatko;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		File folder = new File(".");

		List<String> txtFiles = Arrays.stream(folder.listFiles(new CustomFileFilter("txt")))
				.map(f -> f.getName())
				.toList();
		
		System.out.println(txtFiles);

	}
}

class CustomFileFilter implements FileFilter {

	private String[] arr;

	public CustomFileFilter(String... arr) {
		super();
		this.arr = arr;
	}

	private boolean check(String ext) {
		for (String stringExt : arr) {
			if (stringExt.equals(ext)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean accept(File pathname) {
		int pointerIndex = pathname.getName().lastIndexOf(".");
		if (pointerIndex == -1) {
			return false;
		}
		String ext = pathname.getName().substring(pointerIndex + 1);
		return check(ext);
	}

}