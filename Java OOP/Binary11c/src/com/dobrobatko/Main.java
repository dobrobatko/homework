package com.dobrobatko;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {

		List<Integer> list1 = List.of(5, 0, 3, 4);
		List<Integer> list2 = List.of(10, -2, 5);

		BinaryOperator<List<Integer>> biOne = Main::minimal;

		System.out.println(biOne.apply(list1, list2));

		List<String> list3 = List.of("I", "like", "Java");
		List<String> list4 = List.of("You", "like", "Java");

		BinaryOperator<List<String>> biTwo = Main::minimal;

		System.out.println(biTwo.apply(list3, list4));

	}

	public static <T extends Comparable<? super T>> List<T> minimal(List<T> list1, List<T> list2) {
		Comparator<T> comp = Comparator.<T>naturalOrder();
		T min1 = Collections.min(list1, comp);
		T min2 = Collections.min(list2, comp);

		if (min1.compareTo(min2) < 0) {
			return list1;
		}
		if (min1.compareTo(min2) > 0) {
			return list2;
		}
		return null;

	}
}
