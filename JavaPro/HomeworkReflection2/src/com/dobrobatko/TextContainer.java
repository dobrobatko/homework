package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

@SaveTo(path = "file.txt")
public class TextContainer {
	private String hidden = "It`s private string.";

	public TextContainer() {
		super();
	}
	
	@Save
	public void save(String path) {
		File file = new File(path);
		 try(PrintWriter pw = new PrintWriter(file)) {
			 pw.println(this.hidden);
		 } catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "TextContainer [Private string: " + hidden + "]";
	}
	
	
}
