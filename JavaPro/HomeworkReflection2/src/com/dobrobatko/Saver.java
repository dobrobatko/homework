package com.dobrobatko;

import java.lang.reflect.Method;

public class Saver {

	public Saver() {
		super();
	}

	public static void start() {
		final Class<?> cls = TextContainer.class;
		if (cls.isAnnotationPresent(SaveTo.class)) {
			SaveTo st = cls.getAnnotation(SaveTo.class);
			String path = st.path();
			Method[] methods = cls.getMethods();
			for (Method method : methods) {
				if (method.isAnnotationPresent(Save.class)) {
					TextContainer tc = new TextContainer();
					try {
						method.invoke(tc, path);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
	}

}
