package com.dobrobatko;

import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) {
		final Class<?> cls = Counter.class;
		int result = 0;
		
		Method[] methods = cls.getMethods();
		for (Method method : methods) {
			if(method.isAnnotationPresent(Test.class)) {
				Test an = method.getAnnotation(Test.class);
				Counter obj = new Counter();
				try {
				result = (int) method.invoke(obj, an.param1(), an.param2());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(result);

	}

}
