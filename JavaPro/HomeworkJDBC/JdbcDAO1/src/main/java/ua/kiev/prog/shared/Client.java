package ua.kiev.prog.shared;

public class Client {
    @Id
    private int id;

    private String name;
    private int age;

    public Client() {
    }

    public Client(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Client{");
        if (id != 0) {
            if (name == null && age == 0) {
                sb.append("id=" + id);
            } else {
                sb.append("id=" + id + ", ");
            }
        }
        if (name != null) {
            if (age == 0) {
                sb.append("name=" + name);
            } else {
                sb.append("name=" + name + ", ");
            }
        }
        if (age != 0) sb.append("age=" + age);

        sb.append('}');
        return sb.toString();
    }
}
