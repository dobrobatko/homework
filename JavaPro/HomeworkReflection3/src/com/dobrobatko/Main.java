package com.dobrobatko;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {

		Cat catOne = new Cat("Kuzia", 3, "grey", 2);
		System.out.println(catOne);

		Map<String, Object> data = getFieldsToSave(catOne);

		// Write fields to file
		Container toSave = new Container(data);
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data.dat"))) {
			oos.writeObject(toSave);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Read fields from file
		File file = new File("data.dat");
		Container load = null;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
			try {
				load = (Container) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String, Object> loaded = load.getList();

		// Extract and set fields
		Cat catTwo = new Cat();
		setFields(catTwo, loaded);

		System.out.println(catTwo);

	}

	public static Map<String, Object> getFieldsToSave(Cat cat) {
		Map<String, Object> result = new HashMap<>();
		final Class<?> cls = Cat.class;

		Field[] fields = cls.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Save.class)) {
				String fieldName = field.getName();
				field.setAccessible(true);
				try {
					Object value = field.get(cat);
					result.put(fieldName, value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}

			}
		}
		return result;
	}

	public static void setFields(Cat cat, Map<String, Object> fields) {
		final Class<?> cls = Cat.class;
		for (Map.Entry<String, Object> entry : fields.entrySet()) {
			String key = entry.getKey();
			Object val = entry.getValue();
			try {
				Field field = cls.getDeclaredField(key);
				field.setAccessible(true);
				try {
					field.set(cat, val);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}

		}
	}

}
