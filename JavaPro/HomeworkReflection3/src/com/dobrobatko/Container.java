package com.dobrobatko;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Container implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> list = new HashMap<>();

	
	public Container() {
		super();
	}


	public Container(Map<String, Object> list) {
		super();
		this.list = list;
	}


	public Map<String, Object> getList() {
		return list;
	}
	
	
	
	
	
}
