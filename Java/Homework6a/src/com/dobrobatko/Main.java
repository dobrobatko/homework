package com.dobrobatko;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		/* Max number in array*/
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Input size: ");
		int size = sc.nextInt();
		
		int [] arrayOne = getRandomArray(size);
		
		System.out.println(Arrays.toString(arrayOne));
		
		System.out.println("Maximum of this array is: " + (findMaximum(arrayOne)));
		
		/* "(Integer + Double) + String" method */
		
		Scanner in = new Scanner(System.in);
		System.out.println("Input Integer: ");
		int intgr = sc.nextInt();
		System.out.println("Input Double: ");
		double dbl = sc.nextDouble();
		System.out.println("Input String: ");
		String strng = in.nextLine();
		
		System.out.println(doConcat(intgr, dbl, strng));
		
		/* Draw custom rectangle */
		
		System.out.println("Input width: ");
		int w = sc.nextInt();
		System.out.println("Input height: ");
		int h = sc.nextInt();
		
		drawRectangle(w, h);
		
		/* Finding element in array*/
		
		System.out.println("Input size: ");
		size = sc.nextInt();
		
		int [] arrayTwo = getRandomArray(size);
		
		System.out.println(Arrays.toString(arrayTwo));
		
		System.out.println("Input element to find: ");
		int element = sc.nextInt();
		
		int index = findElement(arrayTwo, element);
		
		System.out.println("Element " + element +" found on index: " + index);
		
		/* Word counter*/
		
		System.out.println("Input string: ");
		String text = in.nextLine();
		System.out.println("This string have " + countWord(text) + " words.");
		
		

	}
	
	static int [] getRandomArray(int size) {
		int [] tempArray = new int [size];
		for (int i = 0; i < tempArray.length; i++) {
			tempArray[i] = (int)(Math.random() * 100);
		}
		return tempArray;
	}
	
	static int findMaximum (int [] tempArray) {
		int maximum = tempArray[0];
		for (int element : tempArray) {
			if (element > maximum) {
				maximum = element;
			}
		}
		return maximum;
	}
	
	static String doConcat (int i, double d, String s) {
		double summ = i + d;
		String output = ( s + summ );
		return output;
	}
	
	static void drawRectangle (int width , int height) {
		for (int i = 0; i < height; i++ ) {
			for (int j = 0; j < width; j++ ) {
				if (i == 0 || i == height - 1) {
					System.out.print("#");
				} else {
					if (j == 0 || j == width -1) {
						System.out.print("#");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}
	
	static int findElement (int [] array, int element) {
		int index = -1;
		for (int i = 0; i < array.length; i++) {
			if (element == array[i]) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	static int countWord (String text) {
		int counter = 1;
		char [] symbols = text.toCharArray();
		for (char element : symbols) {
			if (element == ' ') {
				counter++;
			}
		}
		return counter;
	}

}
