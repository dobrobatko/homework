package com.gmail.dobrobatko;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		/* Wallpaper */
		
		System.out.println("Input bands quantity: ");
		int q = sc.nextInt();
		
		for (int i = 1; i < 6; i++ ) {
			for (int j = 1; j <= q; j++) {
				if (j%2 == 0) {
					System.out.print("+++");
				} else {
					System.out.print("***");
				}
			}
			System.out.println();
		}
		
		/* Factorial */
		
		long fact;
		
		System.out.println("Input n (4<n<16): ");
		int n = sc.nextInt();
		if (n<=4 || n>=16) {
			System.out.println("Incorrect input. Number must be from 4 to 16. Please, try again.");
		} else {
			fact = n;
			for (int i = n-1; i > 0; i--) {
				fact = fact * i;
			}
			System.out.println(n + "! = " + fact);
		}
		
		/* multiplication by 5 */
		
		int result;
		
		for (int i = 1; i <10; i++) {
			result = i * 5;
			System.out.println("5 x " + i + " = " + result);
		}
		
		/* Custom rectangle */
		
		System.out.println("Input width of the rectangle: ");
		int w = sc.nextInt();
		System.out.println("Input height of the rectangle: ");
		int h = sc.nextInt();
		
		for (int i = 1; i <= h; i++) {
			System.out.print("*");
			for (int j = 2; j <= w; j++) {
				if (i == 1 || i == h || j == w) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
