package com.gmail.dobrobatko;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		/* Custom triangle. One cycle. */
		
		System.out.println("Input height of the triangle: ");
		int tH = sc.nextInt();
		
		int inc = 1;
		int dec = tH;
		int reset = 1;
		
		for (int i = 1; i <= tH * tH; i++) {
			System.out.print("*");
			
			if (i == 1 || i == reset) {
				System.out.println();
				if (inc < tH) {
					reset = reset + ++inc;
				} else {
					reset = reset + --dec;
				}		
			}
		}
		
		/* Simple numbers */
		
		boolean flag = true;
		
		for (int i = 2; i <= 100; i++) {
			flag = true;
			for (int j = 2; j < 10; j++) {
				if (i%j == 0 && i != j) {
					flag = false;
				}
			}
			if(flag) {
				System.out.print(i + ", ");
			}
		}
		
		System.out.println();
		
		/* Hourglass */
		
		System.out.println("Input width of the hourglass (odd number): ");
		int width = sc.nextInt();
		
		if (width%2 != 0) {
			for (int i = 1; i <= width; i++) {
				for (int j = 1; j <= width; j++) {
					if (i == 1 || i == width || i == j || j == width - (i-1)) {
						System.out.print("*");
					} else {
						System.out.print(" ");
					}
				}
				System.out.println();
			}
		} else {
			System.out.println("Wrong number inputed. Must be odd number!");
		}		
	}
}
