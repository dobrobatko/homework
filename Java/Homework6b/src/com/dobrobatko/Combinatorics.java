package com.dobrobatko;

import java.util.Arrays;

public class Combinatorics {

	public static void main(String[] args) {
		
		int [] array = new int []{1,2,3,4,5};
		int counter = getFactorial(array.length);
		int max = array.length - 1;
		int temp;
		int move = max;
		
		System.out.println("We have  " + counter + " variants.");
		
		for (; counter > 0; counter--) {
			temp = array[move];
			array[move] = array[move - 1];
			array[move - 1] = temp;
			System.out.println(Arrays.toString(array));
			if (move < 2) {
				move = max;
			} else {
				move--;
			}
		}	
	}
	
	static int getFactorial(int num) {
		return (num > 0) ? num * getFactorial(num - 1) : 1;
	}

}
