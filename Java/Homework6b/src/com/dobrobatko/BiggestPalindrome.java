package com.dobrobatko;

public class BiggestPalindrome {

	public static void main(String[] args) {
		
		/* 3-digits multiplied biggest palindrome */
		
		long mult;
		long big = -1;
		
		for (int i = 100; i < 1000; i++) {
			for (int j = 100; j < 1000; j++) {
				mult = i * j;
				if (isPalindrome(mult)) {
					big = mult > big ? mult : big;
				}
			}
		}
		System.out.println(big);
	}
	
	static boolean isPalindrome (long input) {
		String text = ("" + input);
		char [] charArray = text.toCharArray();
		int [] original = new int [charArray.length];
		int [] control = new int [charArray.length];
		boolean check = false;
		for ( int i = 0; i < charArray.length; i++) {
			original[i] = Character.getNumericValue(charArray[i]);
			control[i] = original[i];
		}
		int temp;
		for (int i = 0; i < original.length / 2; i++) {
			temp = original[i];
			original[i] = original[original.length - i - 1];
			original[original.length - i - 1] = temp;
		}
		temp = 0;
		for (int i = 0; i < original.length; i++ ) {
			if (original[i] == control[i]) {
				temp++;
			}
		}
		if (temp == original.length) {
			check = true;
		} else {
			check = false;
		}	
		return check;
	}

}
