package com.dobrobatko;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		/* Find odd elements of array*/
		int [] array = new int [] {0,5,2,4,7,1,3,19};
		int odd = 0;
		
		for( int element : array) {
			if (element % 2 != 0) {
				odd++;
			}
		}
		System.out.println("We found " + odd + " odd elements in this array.");
		
		/* Create custom array */
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Input new array size: ");
		int size = sc.nextInt();
		
		int [] arrayCustom = new int [size];
		for( int i = 0 ; i < arrayCustom.length; i++ ) {
			System.out.println("Input value for " + i + " element of our array: ");
			arrayCustom[i] = sc.nextInt();
		}
		System.out.println(Arrays.toString(arrayCustom));

		/* Duplication and extend array*/
		int [] arrayOrigin = new int [15];
		
		for (int i = 0; i < arrayOrigin.length; i++) {
			arrayOrigin[i] = (int)(Math.random() * 10);
		}
		
		int [] arrayExtended = Arrays.copyOf(arrayOrigin, 30);
		
		for (int i = 0; i < arrayOrigin.length; i++) {
			arrayExtended [i + arrayOrigin.length] = arrayOrigin [i] * 2;
		}
		System.out.println(Arrays.toString(arrayExtended));
		
		
		/* 'b' counter */
		System.out.println("Input various string: ");
		Scanner in = new Scanner(System.in);
		
		String text = in.nextLine();
		
		int counter = 0;
		
		char [] symbols = text.toCharArray();
		for (char element : symbols) {
			if (element == 'b') {
				counter++;
			}
		}
		
		System.out.println("We found " + counter + " b-symbols in this string.");
		
		in.close();
		sc.close();
	}

}
