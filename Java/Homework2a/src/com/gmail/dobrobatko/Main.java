package com.gmail.dobrobatko;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		/* Max number */
		
		int n1;
		int n2;
		int n3;
		int n4;
		int max;
		
		System.out.println("Input number 1:");
		n1 = sc.nextInt();
		System.out.println("Input number 2:");
		n2 = sc.nextInt();
		System.out.println("Input number 3:");
		n3 = sc.nextInt();
		System.out.println("Input number 4:");
		n4 = sc.nextInt();
		
		max = n1;
		
		if (n2 > max) {
			max = n2;
			}
		if (n3 > max) {
			max = n3;
			}
		if (n4 > max) {
			max = n4;
			}
		
		System.out.println("Max = " + max);
		
		/* Where your apartment? */
		
		int ap;
		int entrance = 0;
		int flour = 0;
		
		System.out.println("Input number of apartment: ");
		ap = sc.nextInt();
		
		if (ap > 0 && ap < 145) {
		
			if(ap%36 != 0) {
				entrance = ap/36 + 1;
			} else {
				entrance = ap/36;
				}
			
			if((ap - ((entrance - 1) * 36))%4 != 0) {
				flour = (ap - ((entrance - 1) * 36))/4 + 1;
			} else {
				flour = (ap - ((entrance - 1) * 36))/4;
				}
			
			System.out.println("Entrance is: " + entrance);
			System.out.println("Flour is: " + flour);
		} else {
			System.out.println("Unknown apartment!!!");
		}
		
		/* Is this a leap year? */
		
		int year;
		boolean leap = false;
		
		System.out.println("Input year: ");
		year = sc.nextInt();
		
		if (year%4 == 0 && year%100 != 0) {
			leap = true;	
		} 
		else if (year%4 == 0 && year%400 == 0) {
			leap = true;
		}
		else {
			leap = false;
		}
		
		if (leap) {
			System.out.println("Yes! Year " + year + " is leap.");
		} else {
			System.out.println("No! Year " + year + " is not a leap year.");
		}
		
		/* Is there a triangle? */
		
		System.out.println("Input a-side of the triangle:");
		double aSide = sc.nextDouble();
		System.out.println("Input b-side of the triangle:");
		double bSide = sc.nextDouble();
		System.out.println("Input c-side of the triangle:");
		double cSide = sc.nextDouble();
		
		if (((aSide + bSide) > cSide) && ((cSide + bSide) > aSide) && (cSide + aSide) > bSide) {
			System.out.println("The triangle exists.");
		} else {
			System.out.println("The triangle DOESN`T exist.");
		}	
	}
}
