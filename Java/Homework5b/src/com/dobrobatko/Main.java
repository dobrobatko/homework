package com.dobrobatko;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		   /* 
		   Create original array: 
		   1 2 3 4 5 6
		   1 2 3 4 5 6
		   1 2 3 4 5 6
		   1 2 3 4 5 6
		   1 2 3 4 5 6
		   1 2 3 4 5 6
		   */
		int [] [] array = new int [6] [6];
		
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = j + 1;
			}
		}
		
		for (int [] row : array) {
			System.out.println(Arrays.toString(row));
		}
		System.out.println();
		
		System.out.println("Rotate to 90 degrees \n");
		
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = i+1;
			}
		}
		
		for (int [] row : array) {
			System.out.println(Arrays.toString(row));
		}
		System.out.println();
		
		System.out.println("Rotate to 180 degrees \n");
		
		for (int i = array.length-1; i >= 0; i--) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = array.length - j;
			}
		}
		
		for (int [] row : array) {
			System.out.println(Arrays.toString(row));
		}
		System.out.println();
		
		System.out.println("Rotate to 270 degrees \n");
		
		for (int i = array.length-1; i >= 0; i--) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = array.length - i;
			}
		}
		
		for (int [] row : array) {
			System.out.println(Arrays.toString(row));
		}
		System.out.println();
		
		/* Mirror array */
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Input size of new array: ");
		int size = sc.nextInt();
		
		int [] original = new int [size];
		
		for (int i = 0; i < original.length; i++) {
			original[i] = (int)(Math.random() * 10);
		}
		
		System.out.println("Original array: ");
		System.out.println(Arrays.toString(original));
		
		int temp;
		
		for (int i = 0; i < original.length / 2; i++) {
			temp = original[i];
			original[i] = original[original.length - i - 1];
			original[original.length - i - 1] = temp;
		}
		
		System.out.println("Mirror array: ");
		System.out.println(Arrays.toString(original));
		
		/* Money to String */
		// Will be later. I working on it...
			
	}

}
