package com.dobrobatko;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		// Simple text editor
		String text = inputText();
		saveTextToFile(text);
		
		// 2D array to file
		int [][] array = { { 1 , 2 , 3 }, { 4 , 5 , 6 } };
		saveArrayToFile(array);
		
		// List of folders only
		Scanner sc = new Scanner(System.in);
		System.out.println("Input folder name: ");
		String name = sc.nextLine();
		File folder = new File (name);
		foldersList(folder);

	}
	
	public static String inputText () {
		Scanner in = new Scanner(System.in);
		String text = "";
		StringBuilder sb = new StringBuilder();
		System.out.println("Input new text. Write 'exit' for save and quit.");
		while (text.equals("exit") == false) {
			text = in.nextLine();
			if (text.equals("exit") == false) {
				sb.append(text);
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}
	
	public static void saveTextToFile (String text) {
		String name = "document.txt";
		File file = new File (name);
		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(text);
			System.out.println("Saved successfully in " + name);
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
	public static void saveArrayToFile (int [][] array) {
		StringBuilder sb = new StringBuilder();
		File file = new File ("array.txt");
		for (int[] rows : array) {
			for (int col : rows) {
				sb.append(String.format("%-6.6s\t", "" + col));
			}
			sb.append(System.lineSeparator());
		}
		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(sb.toString());
			System.out.println("2D Array saved successfully!");
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
	public static void foldersList (File folder) {
		if (folder.isDirectory()) {
			File[] folders = folder.listFiles();
			for (File f : folders) {
				if (f.isDirectory()) {
					System.out.println(f);
				}
			}
		} else {
			System.out.println("Error: Is not a folder");
		}
	}
}
