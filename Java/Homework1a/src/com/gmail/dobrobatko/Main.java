package com.gmail.dobrobatko;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		/* Parsing five-digit number */
			System.out.println("Input five-digit number:");
			int number = sc.nextInt();
		
			int a = number / 10000;
			int b = number % 10000 / 1000;
			int c = number % 1000 / 100;
			int d = number % 100 / 10;
			int e = number % 10;
		
			System.out.println(a);
			System.out.println(b);
			System.out.println(c);
			System.out.println(d);
			System.out.println(e);

		/* Calculating the area of a triangle */
			System.out.println("Input a-side of the triangle:");
			double aSide = sc.nextDouble();
			System.out.println("Input b-side of the triangle:");
			double bSide = sc.nextDouble();
			System.out.println("Input c-side of the triangle:");
			double cSide = sc.nextDouble();
			
			double p = (aSide + bSide + cSide) / 2.0;
			
			double area = Math.sqrt(p * (p - aSide) * (p - bSide) * (p - cSide));
			
			System.out.println("Area of this triangle is: " + area);
			
		/*	Calculating of the circumference. */
			System.out.println("Input radius of the circle:");
			double radius = sc.nextDouble();
			
			double cf = 2 * Math.PI * radius;
			
			System.out.println("The circumference is: " + cf);
	}

}
