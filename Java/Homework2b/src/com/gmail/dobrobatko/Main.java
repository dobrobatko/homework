package com.gmail.dobrobatko;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		/* Point in the circle. */
		
		double radius = 4;
		double x0 = 0;
		double y0 = 0;
		double x;
		double y;
		
		System.out.println("Input x: ");
		x = sc.nextDouble();
		System.out.println("Input y: ");
		y = sc.nextDouble();
		
		if(Math.pow((x-x0), 2) + Math.pow((y-y0), 2) <= Math.pow(radius, 2)) {
			System.out.println("This point IN the circle.");
		} else {
			System.out.println("This point OUT of the circle.");
		}
		
		/* Point in the triangle. */
		
		double xA = 0;
		double yA = 0;
		double xB = 4;
		double yB = 4;
		double xC = 6;
		double yC = 1;
		double ab;
		double bc;
		double ca;
		double xD;
		double yD;
		double pp;
		double abcS;
		double da;
		double db;
		double dc;
		double abdS;
		double bcdS;
		double adcS;
		
		ab = Math.sqrt(Math.pow(xB - xA , 2) + Math.pow(yB - yA , 2));
		bc = Math.sqrt(Math.pow(xC - xB , 2) + Math.pow(yC - yB , 2));
		ca = Math.sqrt(Math.pow(xA - xC , 2) + Math.pow(yA - yC , 2));
		
		pp = (ab + bc + ca) / 2.0;
		
		abcS = Math.sqrt(pp * (pp - ab) * (pp - bc) * (pp - ca));
		
		System.out.println("Input X coordinate of D-point: ");
		xD = sc.nextDouble();
		System.out.println("Input Y coordinate of D-point: ");
		yD = sc.nextDouble();
		
		da = Math.sqrt(Math.pow(xA - xD , 2) + Math.pow(yA - yD, 2));
		db = Math.sqrt(Math.pow(xB - xD , 2) + Math.pow(yB - yD, 2));
		dc = Math.sqrt(Math.pow(xC - xD , 2) + Math.pow(yC - yD, 2));
		
		// ABD triangle
		pp = (ab + db + da) / 2.0;
		abdS = Math.sqrt(pp * (pp - ab) * (pp - db) * (pp - da));
		// BCD triangle
		pp = (bc + dc + db) / 2.0;
		bcdS = Math.sqrt(pp * (pp - bc) * (pp - dc) * (pp - db));
		// ADC triangle
		pp = (da + dc + ca) / 2.0;
		adcS = Math.sqrt(pp * (pp - da) * (pp - dc) * (pp - ca));
		
		if (abdS + bcdS + adcS <= abcS) {
			System.out.println("This point IN the triangle.");
		} else {
			System.out.println("This point OUT of the triangle.");
		}
		
		/* Happy ticket */
		
System.out.println("Input 4-digits ticket`s number: ");
		int ticket = sc.nextInt();
		
		int d1 = ticket / 1000;
		int d2 = ticket % 1000 / 100;
		int d3 = ticket % 100 / 10;
		int d4 = ticket % 10;
		
		int left = d1 + d2;
		int right = d3 + d4;
		
		if (left >= 10) {
			d1 = left / 10;
			d2 = left % 10;
			left = d1 + d2;
		}
		
		if (right >= 10) {
			d3 = right / 10;
			d4 = right % 10;
			right = d3 + d4;
		}
		
		if (left == right) {
			System.out.println("Your ticket - HAPPY!!!");
		} else {
			System.out.println("Not now :( Try another ticket. Good luck! ");
		}
		
		/* Palindrome check */
		
		int a;
		int b;
		int c;
		int d;
		int e;
		int f;
		
		System.out.println("Input 6-digits number for check: ");
		int number = sc.nextInt();
		
		a = number / 100000;
		b = number % 100000 / 10000;
		c = number % 10000 / 1000;
		d = number % 1000 / 100;
		e = number % 100 / 10;
		f = number % 10;

		if (a == f && b == e && c == d) {
			System.out.println("This number is palindrome.");
		} else {
			System.out.println("This number is NOT palindrome.");
		}
	}
}
