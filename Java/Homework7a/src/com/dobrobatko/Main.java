package com.dobrobatko;

import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		/* Get milliseconds*/
		
		Calendar currentDate = Calendar.getInstance();
		Calendar monthagoDate = Calendar.getInstance();
		
		int month = currentDate.get(Calendar.MONTH);
		int monthago = month - 1;
		
		monthagoDate.set(Calendar.MONTH, monthago);
		
		Date now = currentDate.getTime();
		Date prev = monthagoDate.getTime();
		
		long delta = now.getTime() - prev.getTime();
		
		System.out.println(delta);
		
		/* Own method Arrays.toString */
		
		int [] array = new int[] {1,2,3,4,5,6,7,8,9};
		System.out.println(arrayToString(array));
		
		/* Binary to decimal */
		
		Scanner in = new Scanner(System.in);
		System.out.println("Input number in binary: ");
		String input = in.nextLine();
		System.out.println("Decimal is: " + getDecimal(input));
		
		/* Many PIs */
		
		for (int i = 2; i <= 11; i++) {
			System.out.println(piFormat(i));
		}
	}
	
	static String arrayToString (int [] array) {
		StringBuilder sb = new StringBuilder("[ ");
		for (int i = 0; i < array.length; i++) {
			if (i < array.length - 1) {
				sb.append(array[i] + ", ");
			} else {
				sb.append(array[i] + " ]");
			}
		}
		String text = sb.toString();
		return text;
	}
	
	static int getDecimal (String text) {
		int length = text.length();
		StringBuilder sb = new StringBuilder();
		sb.append(text);
		sb.reverse();
		int summ = 0;
		for (int i = 0; i < length; i++) {
			if(sb.charAt(i) == '1') {
				summ += (int)(Math.pow(2, i));
			}
		}
		return summ;
	}
	
	static String piFormat (int i) {
		String output;
		Formatter form = new Formatter();
		form.format("%."+i+"f", Math.PI);
		output = form.toString();
		return output;
	}

}
