    // створюємо конструктор
    function Human(name, age) { 
        this.name = name; 
        this.age = age;

        // змінні та методи на рівні конструктора
        var input = new Date();
        var year = input.getFullYear(); 
        
        // розраховуємо властивість об'єкта "рік народження"
        this.yearOfBirth = year - this.age; 

        // константа на рівні конструктора
        var MAX_AGE = 100; 
        
        
        this.howLong = function() {
                            document.write(this.name + ' жити до 100 ще ' + calc() + ' років! </br>');         
                        }
        
        // збереження замикання
        var self = this;  
        // метод на рівні конструктора, недосяжний "зовні"
        var calc = function(){          
                        return MAX_AGE - self.age;
                    }       
    }

var check = true;
var person = 1;
var dataBase = new Array();

    // заповнюємо масив об'єктами
    while(check) {      
        var n = prompt('Вкажіть ім^я ' + person + ' людини:');
        var a = prompt('Вкажіть вік ' + person + ' людини:');
        dataBase[person - 1] = new Human(n, a);
        person++;
        if((n == null || a == null) || (n == "" || a == "")) check = !true;
    }

    // видаляємо зайвий останній порожній елемент масиву
    dataBase.pop(); 
    
    // <------- Функції сортуваня об'єктів за віком --------------
    function sortByAgeUp(array) {
        array.sort((a, b) => a.age - b.age);
    }

    function sortByAgeDown(array) {
        array.sort((a, b) => b.age - a.age);
    }
    // ----------------------------------------------------------->

    // Функція відображення масиву
    function showArray(array) {
        var arr = array;
        for(var i = 0; i < arr.length; i++) {
            document.write(arr[i].name + ' - ' + arr[i].age + ' років </br>');
        }
        document.write('<hr>');
    }

    // Функція відображення року народження
    function birthYear(array) {
        var arr = array;
        for(var i = 0; i < arr.length; i++) {
            document.write(arr[i].name + ' - ' + arr[i].yearOfBirth + ' року народження (але не стовідсотково) </br>');
        }
        document.write('<hr>');
    }

    // Функція, де використовується метод екземпляру об'єкту
    function toHundred(array) {
        var arr = array;
        for(var i = 0; i < arr.length; i++) arr[i].howLong();
        document.write('<hr>');
    }

    // Відображення документу
    document.write('<h3>Початковий масив об`єктів</h3>');
    showArray(dataBase);

    document.write('<h3>Відсортований за зростанням віку</h3>');
    sortByAgeUp(dataBase);
    showArray(dataBase);
  
    document.write('<h3>Відсортований за спаданням віку</h3>');
    sortByAgeDown(dataBase);
    showArray(dataBase);

    document.write('<h3>Роки народження</h3>');
    birthYear(dataBase);

    document.write('<h3>Залишилось до ста років</h3>');
    toHundred(dataBase);
  
    

