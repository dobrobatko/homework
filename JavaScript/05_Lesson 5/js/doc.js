
// Задаємо об'єкти Документ та Додаток в Документі
var doc = {
    header: '',
    body: '',
    footer: '',
    date: ''
};

doc.att = {
    header: '',
    body: '',
    footer: '',
    date: ''
};

// Глобально отримуємо дату на сьогодні
var input = new Date();
var data = String(input.getDate()).padStart(2, '0') + '/' + String(input.getMonth() + 1).padStart(2, '0') + '/' + input.getFullYear();


// Метод заповнення основного Документу
    doc.addMain = function () {
        var word;
        var desc;
        var check = !true;
        
        for (key in this) {
            switch (key) {
                case 'header':
                    word = 'заголовок';
                    desc = 'Заява'
                    check = true;
                    break;
                case 'body':
                    word = 'зміст';
                    desc = 'Прошу надати мені відпустку.'
                    check = true;
                    break;
                case 'footer':
                    word = 'підпис';
                    desc = 'П.І.Б.'
                    check = true;
                    break;
                case 'date':
                    word = 'дату';
                    desc = data;
                    check = true;
                    break;
                default: check = !true;
            }
        
        if (check) this[key] = prompt(`Вкажіть ${word} основного документу:`, desc);
        }
    }

// Метод заповнення Додатку
    doc.addAtt = function () {
        var word;
        var desc;
        var check = !true;

        for (key in this.att) {
            switch (key) {
                case 'header':
                    word = 'заголовок';
                    desc = 'Пояснення'
                    check = true;
                    break;
                case 'body':
                    word = 'зміст';
                    desc = 'Тому шо треба.'
                    check = true;
                    break;
                case 'footer':
                    word = 'підпис';
                    desc = 'П.І.Б.'
                    check = true;
                    break;
                case 'date':
                    word = 'дату';
                    desc = data;
                    check = true;
                    break;
                default: check = !true;
            }
        
        if (check) this.att[key] = prompt(`Вкажіть ${word} додатку:`, desc);
        }
    }


// Методи виведення інформації з Документу та Додатку    
    doc.printMain = function () {
        document.write(`<div>
            <h1>${this.header}</h1>
            <h2>${this.body}</h2>
            <h3>${this.footer}</h3>
            <h3>${this.date}</h3>
            </div>`);

    }

    doc.printAtt = function () {
        document.write(`<div>
            <h1>${this.att.header}</h1>
            <h2>${this.att.body}</h2>
            <h3>${this.att.footer}</h3>
            <h3>${this.att.date}</h3>
            </div>`);

    }


    doc.addMain();
    doc.addAtt();

    doc.printMain();
    doc.printAtt();
