window.onload = () => {
    const btnPrevious = document.getElementById('previous');
    const btnNext = document.getElementById('next');
    const photos = document.querySelectorAll('main .photobank img');
    let counter = 0;

        btnPrevious.onclick = () => {
            photos[counter].style.display = 'none';
            counter--;
            if(counter < 0) counter = photos.length - 1;
            photos[counter].style.display = 'block'; 
        }

        btnNext.onclick = () => {
            photos[counter].style.display = 'none';
            counter++;
            if(counter >= photos.length) counter = 0;
            photos[counter].style.display = 'block';   
        }
}