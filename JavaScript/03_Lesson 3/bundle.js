var styles = ['Джаз', 'Блюз']; // объявляем массив, задаем значения

styles.push('Рон-н-ролл'); // добавляем элемент в конец
alert( styles.join(' '));

styles.splice(styles.length / 2, 1, 'Классика'); // меняем элемент в середине массива
alert( styles.join(' '));

alert( styles.shift()); // удаляем и выводим первый элемент массива

styles.unshift('Рэп','Регги'); // добавляем элементы в начало массива
alert( styles.join(' '));