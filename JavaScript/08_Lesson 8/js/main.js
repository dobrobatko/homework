window.onload = () => {
    
    let count = 0;

    const newdivs = new Array(10);
    const div = document.getElementById('box');
    const addButton = document.getElementById('button');
            
        addButton.onclick = () => {

            if(count == 10) {
                window.location.reload();
                return;
            }
            
            newdivs[count] = document.createElement("div");

            newdivs[count].style.width = "50px";
            newdivs[count].style.height = "50px";
            newdivs[count].style.backgroundColor = "yellow";
            newdivs[count].style.fontSize = "2em";
            newdivs[count].style.textAlign = "center";
            newdivs[count].style.margin = "5px";
            newdivs[count].style.display = "inline-block";
            
            newdivs[count].innerHTML = `${count + 1}`;
                
            div.appendChild(newdivs[count]);
            count++;              
        }  
}