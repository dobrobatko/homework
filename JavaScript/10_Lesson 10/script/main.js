window.onload = () => {
    
    document.body.style.paddingLeft = '0';
    document.body.style.marginLeft = '0';

    const startBtn = document.getElementById('button');
    let controlFlag = false;
        
        startBtn.addEventListener('click',
            function() {
                
                const input = document.createElement('input');
                input.setAttribute('type', 'number');
                input.setAttribute('id', 'input');
                input.setAttribute('value', '100');
                input.style.textAlign = 'right';
                

                const submitBtn = document.createElement('input');
                submitBtn.setAttribute('type', 'button');
                submitBtn.setAttribute('value', 'Draw!');
                submitBtn.setAttribute('id', 'submit');
                

                document.body.append('Enter diameter: ');
                document.body.append(input);
                document.body.append(' px ');
                document.body.append(submitBtn);
                

                startBtn.style.display = 'none';

                submitBtn.addEventListener('click',
                    function() {

                        if(controlFlag) return alert('The task comlete already. Reload page for the new drawn.');
                        controlFlag = true;

                        const windowInnerWidth = document.documentElement.clientWidth;
                        let maxDiameter = windowInnerWidth / 10;

                            if(input.value >= maxDiameter) {
                                alert(`Diameter must be less than 1/10 of window width. Please, enter a value no greater than ${maxDiameter} px.`);
                                controlFlag = false;
                                return;
                            }

                            const wrapper = document.createElement('div');
                            wrapper.setAttribute('id', 'wrapper');
                            wrapper.style.width = `${input.value * 10}px`;
                            wrapper.style.marginTop = '10px';
                            wrapper.style.marginLeft = '0px';
                            wrapper.style.paddingLeft = '0px';
                            document.body.append(wrapper);

                            const circles = new Array(100);
                            for (let i = 0; i < circles.length; i++) {
                                circles[i] = document.createElement('div');
                                circles[i].style.width = `${input.value}px`;
                                circles[i].style.height = `${input.value}px`;
                                circles[i].style.display = 'inline-block';
                                circles[i].style.backgroundColor = randomColor();
                                circles[i].style.borderRadius = '50%';
                                wrapper.append(circles[i]);
                            }

                            

                            wrapper.addEventListener('click', (e) => {
                                e.target.style.display = 'none';    
                            });
                        
                })});

         let randomColor = () => {
                                function random(min, max) {
                                    let rand = min + Math.random() * (max + 1 - min);
                                    return Math.floor(rand);
                                }
                                let h = random(0,360);
                                return (`hsl(${h}, 70%, 50%)`);
                            };          
}