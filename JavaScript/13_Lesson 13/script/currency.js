window.onload = () => {

    let today = new (Date);
        today = (`${today.getDate()}.${today.getMonth()+1}.${today.getFullYear()}`);
        document.querySelectorAll('p')[1].innerHTML = today;

    let table = document.querySelector('table'); 
        table.innerHTML = '<tr><th>Валюта</th><th>Вартість, грн.</th></tr>'
    const less = document.querySelectorAll('input')[0];
    const more = document.querySelectorAll('input')[1];

    let xhr = new XMLHttpRequest();
        xhr.open("GET","https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
        xhr.send();
        setTimeout(function() {
            if(xhr.readyState == 4 && xhr.status == 200) {
                let currencyData = JSON.parse(xhr.responseText);
                    outputAll(currencyData);
                    less.addEventListener('click', () => {lessSort(currencyData)});
                    more.addEventListener('click', () => {moreSort(currencyData)});
            } else {alert ('Server not responding!')}
        }, 3000);

       

    let outputAll = (array) => {
        for(let i=0; i < array.length; i++) {
            
            let row = document.createElement('tr');
                row.innerHTML = `<td>${array[i].txt}</td><td>${(array[i].rate).toFixed(2)}</td>`;
            table.append(row);
        }
    }

    let lessSort = (array) => {
        table.innerHTML = '';
        table.innerHTML = '<tr><th>Валюта</th><th>Вартість, грн.</th></tr>'
        for(let i=0; i < array.length; i++) {
            if(array[i].rate < 25) {
                    let row = document.createElement('tr');
                    row.innerHTML = `<td>${array[i].txt}</td><td>${(array[i].rate).toFixed(2)}</td>`;
                table.append(row);
            }
        }
    }

    let moreSort = (array) => {
        table.innerHTML = '';
        table.innerHTML = '<tr><th>Валюта</th><th>Вартість, грн.</th></tr>'
        for(let i=0; i < array.length; i++) {
            if(array[i].rate >= 25) {
                    let row = document.createElement('tr');
                    row.innerHTML = `<td>${array[i].txt}</td><td>${(array[i].rate).toFixed(2)}</td>`;
                table.append(row);
            }
        }
    }
                    

    

}