// Embedded class "Hamburger"

// <---- Start of module ---->
// Sintaxis: "new Hamburger(size: "small" || "large", stuffing: "cheese" || "potato" || "salad")"
class Hamburger {
    constructor(size, stuffing) { 
        const SIZE_SMALL = [50, 20]; //[uah, calories]
        const SIZE_LARGE = [100, 40];
        const STUFFING_CHEESE = [10, 20];
        const STUFFING_SALAD = [20, 5];
        const STUFFING_POTATO = [15, 10];
        const TOPPING_MAYO = [20, 5];
        const TOPPING_SPICE = [15, 0];
        let   toppingArr = [];

        // Constructor under control
        try {
            if(!size || !stuffing) {
                throw new SyntaxError('Invalid initialization (SIZE or STUFFING isn`t specified)');
            }
            if(size !== "small" && size !== "large") {
                throw new SyntaxError('Invalid initialization (wrong SIZE specified)'); 
            }
            if(stuffing !== "cheese" && stuffing !== "potato" && stuffing !== "salad") {
                throw new SyntaxError('Invalid initialization (wrong STUFFING specified)'); 
            }

                // Method for adding topping
                this.addTopping = (topping) => {
                    try {
                        if(topping !== "mayo" && topping !== "spice") {
                            throw new SyntaxError('addTopping error (wrong type of TOPPING)'); 
                        }

                        toppingArr.push(topping);

                        if(toppingArr.length > 2) {
                            toppingArr.pop();
                            throw new SyntaxError('addTopping error (too much toppings)');   
                        }
                        if(toppingArr[0] == toppingArr[1]) {
                            toppingArr.pop();
                            throw new SyntaxError('addTopping error (the same toppings)');     
                        }
                    } catch (e) { alert('HamburgerException: ' + e.message);}
                }

                // Method for removing topping
                this.removeTopping = (topping) => {
                    try{
                        if(toppingArr.length == 0) {
                            throw new SyntaxError('removeTopping error (there are no toppings. Removing is not possible.)');    
                        }
                        if(toppingArr[0] !== topping && toppingArr[1] !== topping) {
                            throw new SyntaxError('removeTopping error (there are no SPECIFIED topping. Removing is not possible.)');    
                        }

                        let removeIndex = toppingArr.findIndex(item => item == topping);
                            toppingArr.splice(removeIndex,1);

                    } catch (e) { alert('HamburgerException: ' + e.message);}
                }
                
                // Get array of the toppings
                this.getToppings = () => {
                    return toppingArr;
                }

                // Get size of the hamburger
                this.getSize = () => {
                    return size;
                }

                // Get stuffing of the hamburger
                this.getStuffing = () => {
                    return stuffing;
                }

                // Calculating price
                this.calculatePrice = () => {
                    let summ = 0;
                    switch(size) {
                        case "small" : summ += SIZE_SMALL[0];
                            break;
                        case "large" : summ += SIZE_LARGE[0];
                            break;
                    }
                    switch(stuffing) {
                        case "cheese" : summ += STUFFING_CHEESE[0];
                            break;
                        case "potato" : summ += STUFFING_POTATO[0];
                            break;
                        case "salad" : summ += STUFFING_SALAD[0];
                    }
                    toppingArr.forEach((item) => {
                        switch(item) {
                            case "mayo" : summ += TOPPING_MAYO[0];
                                break;
                            case "spice" : summ += TOPPING_SPICE[0];
                                break;
                        }
                    });
                    return summ;
                }

                // Calculating calories
                this.calculateCalories = () => {
                    let summ = 0;
                    switch(size) {
                        case "small" : summ += SIZE_SMALL[1];
                            break;
                        case "large" : summ += SIZE_LARGE[1];
                            break;
                    }
                    switch(stuffing) {
                        case "cheese" : summ += STUFFING_CHEESE[1];
                            break;
                        case "potato" : summ += STUFFING_POTATO[1];
                            break;
                        case "salad" : summ += STUFFING_SALAD[1];
                    }
                    toppingArr.forEach((item) => {
                        switch(item) {
                            case "mayo" : summ += TOPPING_MAYO[1];
                                break;
                            case "spice" : summ += TOPPING_SPICE[1];
                                break;
                        }
                    });
                    return summ;
                }
            
        } catch (e) { alert('HamburgerException: ' + e.message);}    
    }
}
// <---- End of module ---->

