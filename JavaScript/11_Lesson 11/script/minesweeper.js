window.onload = () => {
    let diff = '';
    let size = 0;
    let bombs = 0;
    let flags = 0;
    let moves = 0;

    const diffBox = document.getElementById('diffBox');
    

        diffBox.addEventListener('click', (e) => {
            diff = e.target.id;
            diffBox.className = '';
            diffBox.style.display = 'none';

            main(diff);
        });

        function main(difficult) {
            
            switch(difficult) {
                case 'easy': size = 8;
                    break;
                case 'normal': size = 10;
                    break;
                case 'hard': size = 16;    
            }
            // Reseting the flags value in case if the Reset button clicked and "main" fuction start again.
            flags = 0;

            // Calculating the bombs and moves for win
            bombs = Math.floor((size*size)/6);
            console.log(bombs);
            moves = (size*size) - bombs;
            console.log(moves);
            
            // Creating Control Panel with Reset Button and Flag/Bomb monitors
            const controlPanel = document.createElement('div');
            controlPanel.className = 'control';
            document.body.append(controlPanel);

            
            // Creating and hide Reset Button
            const resetBtn = document.createElement('button');
            resetBtn.className = 'btn btn-secondary';
            resetBtn.type = 'button';
            resetBtn.innerText = 'Reset Game';
            resetBtn.style.display = 'block';
            resetBtn.style.visibility = 'hidden';
            resetBtn.onclick = () => {
                document.body.removeChild(controlPanel);
                document.body.removeChild(wrapper);
                document.body.removeChild(result);
                alert('New game started!');
                main(diff);
            };
            
            // Adding hidden Reset Button to the Control Panel
            controlPanel.appendChild(resetBtn);

            // Creating flags/bombs monitors
            const monitors = document.createElement('div');
            monitors.style.backgroundColor = 'rgb(110, 110, 110)';
            monitors.style.border = '4px ridge grey';
            monitors.style.padding = '2px';

            controlPanel.appendChild(monitors);
            monitors.innerHTML = `Bombs: ${bombs} || Flags: ${flags}`;

            // Adding Result window
            const result = document.createElement('div');
            result.className = 'result';
            
            // Creating a wrapper div for the boxes
            const wrapper = document.createElement('div');
            wrapper.style.width = `${29 * size}px`;
            wrapper.style.height = `${29 * size}px`;
            wrapper.style.display = `block`;
            wrapper.style.margin = `auto`;

            
            // Adding the wrapper
            document.body.append(wrapper);

            // Creating new empty 2D (size x size) array for boxes
            let boxArray = new Array(size);  
                for(let row = 0; row < size; row++) {
                    boxArray[row] = new Array(size);
                }
            
            // Adding the boxes
            for(let row = 0; row < size; row++) {
                for(let column = 0; column < size; column++) {
                    boxArray[row][column] = document.createElement('div');
                    boxArray[row][column].className = 'minekeeper';
                    boxArray[row][column].row = row;
                    boxArray[row][column].column = column;
                    boxArray[row][column].bomb = false;
                    boxArray[row][column].flag = false;
                    boxArray[row][column].open = false;
                    boxArray[row][column].nothing = false;

                    wrapper.append(boxArray[row][column]);
                }
            }

            // Placing bombs
                function random(min, max) {
                    let rand = min + Math.random() * (max + 1 - min);
                    return Math.floor(rand);
                }
                let placedBombs = 0; 
                while(placedBombs < bombs) {
                    for(let row = 0; row < size; row++) {
                        for(let column = 0; column < size; column++) {
                            if(!boxArray[row][column].bomb && placedBombs < bombs) {
                                let rand = random(1,100);
                                if(rand % 10 === 0) { // Reduce the probability to 1 out of 10 for a more even placing
                                    boxArray[row][column].bomb = true;
                                    placedBombs++;
                                    // Some cheats for test :) uncomment if they needed. It`s showing all the placed bombs. 
                                    /*boxArray[row][column].style.backgroundImage = 'url(./images/bomb.png)';
                                    boxArray[row][column].style.backgroundColor = 'rgb(105, 105, 105)';*/
                                }
                            }
                        }
                    }
                }
                console.log(placedBombs);

            // Checking the environment of this box
                function environment(row, column) {
                    let counter = 0;
                        try { if(boxArray[row - 1][column - 1].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row - 1][column].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row - 1][column + 1].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row][column - 1].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row][column + 1].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row + 1][column - 1].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row + 1][column].bomb) {counter++;} } catch(err) {};
                        try { if(boxArray[row + 1][column + 1].bomb) {counter++;} } catch(err) {};

                        if(counter > 0) {
                            switch(counter) {
                                case 1: {
                                    boxArray[row][column].style.color = 'lime';
                                    break;
                                }
                                case 2: {
                                    boxArray[row][column].style.color = 'yellow';
                                    break;
                                }
                                case 3: {
                                    boxArray[row][column].style.color = 'aqua';
                                    break;
                                }
                                default: boxArray[row][column].style.color = 'red';
                            }
                            boxArray[row][column].innerHTML = counter;
                        }
                        
                        
                        boxArray[row][column].open = true;
                        boxArray[row][column].style.backgroundColor = 'rgb(105, 105, 105)';
                        // Forced unmarked
                            if(boxArray[row][column].flag) {
                                boxArray[row][column].style.backgroundImage = '';
                                boxArray[row][column].flag = false;
                                flags--;
                                monitors.innerHTML = `Bombs: ${bombs} || Flags: ${flags}`;
                        }

                        // Callback if empty
                        if(counter == 0) {
                            boxArray[row][column].nothing = true;
                            try {
                                if(!boxArray[row - 1][column - 1].nothing)
                                environment(boxArray[row - 1][column - 1].row, boxArray[row - 1][column - 1].column)} catch(err) {};
                            try {
                                if(!boxArray[row - 1][column].nothing)
                                environment(boxArray[row - 1][column].row, boxArray[row - 1][column].column)} catch(err) {};
                            try {
                                if(!boxArray[row - 1][column + 1].nothing)
                                environment(boxArray[row - 1][column + 1].row, boxArray[row - 1][column + 1].column)} catch(err) {};
                            try {
                                if(!boxArray[row][column - 1].nothing)
                                environment(boxArray[row][column - 1].row, boxArray[row][column - 1].column)} catch(err) {};
                            try {
                                if(!boxArray[row][column + 1].nothing)
                                environment(boxArray[row][column + 1].row, boxArray[row][column + 1].column)} catch(err) {};
                            try {
                                if(!boxArray[row + 1][column - 1].nothing)
                                environment(boxArray[row + 1][column - 1].row, boxArray[row + 1][column - 1].column)} catch(err) {};
                            try {
                                if(!boxArray[row + 1][column].nothing)
                                environment(boxArray[row + 1][column].row, boxArray[row + 1][column].column)} catch(err) {};
                            try {
                                if(!boxArray[row + 1][column + 1].nothing)
                                environment(boxArray[row + 1][column + 1].row, boxArray[row + 1][column + 1].column)} catch(err) {};
                            }
                    return;

                }

            
            // Box picked: (for wrapper`s click event - left mouse click)
            let pickBox = function pickBox(event) {
                resetBtn.style.visibility = '';
                 
                // Shot the bomb!!! or not... (see "else")
                if(event.target.bomb) {
                    event.target.style.backgroundImage = 'url(./images/bomb.png)';
                        for(let row = 0; row < size; row++) {
                            for(let column = 0; column < size; column++) {
                                if(boxArray[row][column].bomb && !boxArray[row][column].flag) {
                                    boxArray[row][column].style.backgroundColor = 'rgb(105, 105, 105)';
                                    boxArray[row][column].style.backgroundImage = 'url(./images/bomb.png)';
                                } 
                                if(!boxArray[row][column].bomb && boxArray[row][column].flag) {
                                    boxArray[row][column].style.backgroundImage = '';
                                    flags--;
                                    monitors.innerHTML = `Bombs: ${bombs} || Flags: ${flags}`;
                                }
                            }   
                        }  
                    event.target.style.backgroundColor = 'red';
                    wrapper.removeEventListener('click', pickBox);
                    wrapper.removeEventListener('contextmenu', markBox);
                    wrapper.addEventListener('contextmenu', (event) => event.preventDefault());
                    result.style.color = 'red';
                    result.innerHTML = 'BOOM!!! You lose!!!';
                } else {
                            environment(event.target.row, event.target.column);
                            let passes = 0;
                            for(let row = 0; row < size; row++) {
                                for(let column = 0; column < size; column++) {
                                    if (boxArray[row][column].open) passes++;
                                }
                            }
                            if(passes == moves) {
                                result.style.color = 'green';
                                result.innerText = 'Congratulations!!! You Win!'
                                wrapper.removeEventListener('click', pickBox);
                                wrapper.removeEventListener('contextmenu', markBox);
                                wrapper.addEventListener('contextmenu', (event) => event.preventDefault());
                            }
                        }
                
            }

            // Box marked: (for wrapper`s contextmenu event - right mouse click)
            let markBox = function markBox(event) {
                resetBtn.style.visibility = '';
                let row = event.target.row;
                let column = event.target.column;
                if(!boxArray[row][column].flag && !boxArray[row][column].open) {
                    if(flags < bombs) {
                        // Mark closed box
                        event.target.style.backgroundImage = 'url(./images/mark.png)';
                        event.target.style.backgroundRepeat = 'no-repeat';
                        event.target.flag = true;
                        flags++;
                    }
                } else {
                    if(!boxArray[row][column].open)  { 
                        // Unmark closed box
                        event.target.style.backgroundImage = 'none';
                        event.target.flag = false;
                        flags--;
                    }
                }             
                
                monitors.innerHTML = `Bombs: ${bombs} || Flags: ${flags}`;
                event.preventDefault();
                
            }

            wrapper.addEventListener('click', pickBox);
            wrapper.addEventListener('contextmenu', markBox);

            document.body.append(result);
            
            
            
        }
    
}