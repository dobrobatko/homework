window.onload = () => {

    let secret = 'байрактар';
    let secretArr = secret.split('');
    let tryCount = 9;
    let trueAnswers = 0;
    let monitorArr = new Array(secretArr.length);

    const monitorContent = document.getElementById('word');
    const attemptsContent = document.getElementById('attempts');
    const checkField = document.getElementById('check');
    const image = document.getElementById('bpla');
    
        function getStars(length) {
            let res = '';
                for(let i=0; i < length; i++) {
                    res += '*';
                }
            return res;    
        }

            function hideSecret(arr) {
                for(i = 0; i < arr.length; i++) arr[i] = "*";
        }

        monitorContent.innerHTML = getStars(secretArr.length);
        attemptsContent.innerHTML = tryCount;
        hideSecret(monitorArr);

        document.getElementById('sendLetter').onclick = () => {
            
            let answer = document.getElementById('income').value;
                answer = answer.toLowerCase();
            let check = false;         

                for( let i = 0; i < secretArr.length; i++) {
                    if(answer == secretArr[i]) {
                        monitorArr[i] = answer.toUpperCase();
                        delete secretArr[i];
                        trueAnswers++;
                        check = true;
                    }
                }

                if(check) {
                    checkField.style.backgroundColor = 'green';
                    checkField.innerHTML = 'Ай, молодець!';
                    check = false;
                } else {
                    checkField.style.backgroundColor = 'red'; 
                    checkField.innerHTML = 'Не вгадав!';
                    --tryCount;
                }

                monitorContent.innerHTML = monitorArr.join('');
                attemptsContent.innerHTML = tryCount;

                if(tryCount == 0) {
                    alert('Всьо, Game Over :(');
                    window.location.reload();
                }

                if(trueAnswers == secretArr.length) {
                    image.style.display = "block";            
                }
        }
}