    
    // Creating a Constructor with own methods
    class newUser {
        constructor() {
            this.firstName;
            this.lastName;
            this.birthday;

            this.createNewUser = () => {
                this.firstName = prompt('Enter name: ');
                this.lastName = prompt('Enter surname: ');
                this.birthday = prompt('Enter date of birth: ', 'dd.mm.yyyy');
            }

            this.getLogin = () => {
                let partOne = this.firstName.charAt(0);
                let partTwo = this.lastName;
                return (partOne + partTwo).toLowerCase();
            }

            this.getAge = () => {
                let date = new Date();
                let year = date.getFullYear(); 
                let dateOfBirth = this.birthday.split('.');
                    return year - dateOfBirth[2];
            }

            this.getPassword = () => {
                let partOne = this.firstName.charAt(0);
                let partTwo = (this.lastName).toLowerCase();
                let dateOfBirth = this.birthday.split('.');
                    return partOne + partTwo + dateOfBirth[2]; 
            }
        }
    }

// Creating new object 
const user = new newUser();

// Filling created object using createNewUser function
    user.createNewUser();

    console.log(user);

// Testing other functions
let login = user.getLogin();

    console.log(login);

let age = user.getAge();

    console.log(age);

let password = user.getPassword();

    console.log(password);

// Creating new array with the different data types    
const array = [1, 'hello', 256, false, true, 'world!', user];

// Developing a function for filtering array by type
    function filterBy(arr, type) {
        return arr.filter(item => typeof item === type);
    }  

// Testing this function
    console.log(filterBy(array,'string'));
    console.log(filterBy(array,'object'));
    console.log(filterBy(array,'number'));
    console.log(filterBy(array,'boolean'));

// Well done! Awesome!    