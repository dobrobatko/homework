function main() {  

    // Перевірка віку

    var age = prompt('Скільки Вам років?', 18);

        function checkAge(age) {
            var check = age >= 18 ? true : confirm('Батьки дозволили?'); 

            return check;
        }

        if (checkAge(age) == false) /*throw 'access denied'*/ return;

    // Функції обробки масиву

    var arr = [0, 1, 2, 3, 4, 5, 7, 8, 9];
    var num = 100;

        function addItem(array, number) {               // функція додавання довільного числа до кожного з елементів масиву
    
            for (var i = 0; i < array.length; i++) {
                array[i] = array[i] + number; 
            }

            return array;
        }

        function show(a, b, callBackFunction) {
            var result = callBackFunction(a, b);
            document.write(result);
        }

        show(arr, num, addItem);

}

    main();



